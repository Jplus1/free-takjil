<form class="formKonfirmasiSedekah">
    <div class="modal fade" id="modalKonfirmasiSedekah" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Konfirmasi Sedekah</h5>
            <button type="button" class="btn-close btnCloseKonfirmasiSedekah" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <tbody class="">
                        <tr>
                            <td>Nama Donatur</td>
                            <td class="tdKonfirmasiNamaDonatur"></td>
                        </tr>
                        <tr>
                            <td>Jenis Donasi</td>
                            <td class="tdKonfirmasiJenisDonasi"></td>
                        </tr>
                        <tr>
                            <td>Jumlah Porsi</td>
                            <td class="tdKonfirmasiJumlahPorsi"></td>
                        </tr>
                        <tr>
                            <td>Di-<span class="labelKonfirmasi">Konfirmasi</span> Tanggal<br/>(dd-mm-yyyy)</td>
                            <td class="tdKonfirmasiKonfirmasiAtTgl">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-4 col-md-2">
                                            <input  required="required" name="tgl_d" type="number" class="form-control inputKonfirmasiTgl" value="<?=date("d")?>"/>
                                        </div>
                                        <div class="col-xs-4 col-md-2">
                                            <select  required="required"  name="tgl_m" class="form-control inputKonfirmasiBulan">
                                                <option value="01" <?php if(intval(date("m")) == intval("01")) echo "selected" ?>>Jan</option>
                                                <option value="02" <?php if(intval(date("m")) == intval("02")) echo "selected" ?>>Feb</option>
                                                <option value="03" <?php if(intval(date("m")) == intval("03")) echo "selected" ?>>Mar</option>
                                                <option value="04" <?php if(intval(date("m")) == intval("04")) echo "selected" ?>>Apr</option>
                                                <option value="05" <?php if(intval(date("m")) == intval("05")) echo "selected" ?>>Mei</option>
                                                <option value="06" <?php if(intval(date("m")) == intval("06")) echo "selected" ?>>Jun</option>
                                                <option value="07" <?php if(intval(date("m")) == intval("07")) echo "selected" ?>>Jul</option>
                                                <option value="08" <?php if(intval(date("m")) == intval("08")) echo "selected" ?>>Agu</option>
                                                <option value="09" <?php if(intval(date("m")) == intval("09")) echo "selected" ?>>Sep</option>
                                                <option value="10" <?php if(intval(date("m")) == intval("10")) echo "selected" ?>>Okt</option>
                                                <option value="11" <?php if(intval(date("m")) == intval("11")) echo "selected" ?>>Nov</option>
                                                <option value="12" <?php if(intval(date("m")) == intval("12")) echo "selected" ?>>Des</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-4 col-md-2">
                                            <input required="required"  name="tgl_y" type="number" class="form-control inputKonfirmasiTahun" value="<?=date("Y")?>"/>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Di-<span class="labelKonfirmasi">Konfirmasi</span> Jam<br/>(hh:mm)</td>
                            <td class="tdKonfirmasiKonfirmasiAtJam">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-4 col-md-2">
                                            <select   required="required" name="tgl_h" class="form-control inputKonfirmasiJam">
                                                <option value="01" <?= intval(date("H")) == intval("01") ? "selected" : "" ?> >01 <?=date("Y-m-d H:i")?></option>
                                                <option value="02" <?= intval(date("H")) == intval("02") ? "selected" : "" ?> >02</option>
                                                <option value="03" <?= intval(date("H")) == intval("03") ? "selected" : "" ?> >03</option>
                                                <option value="04" <?= intval(date("H")) == intval("04") ? "selected" : "" ?> >04</option>
                                                <option value="05" <?= intval(date("H")) == intval("05") ? "selected" : "" ?> >05</option>
                                                <option value="06" <?= intval(date("H")) == intval("06") ? "selected" : "" ?> >06</option>
                                                <option value="07" <?= intval(date("H")) == intval("07") ? "selected" : "" ?> >07</option>
                                                <option value="08" <?= intval(date("H")) == intval("08") ? "selected" : "" ?> >08</option>
                                                <option value="09" <?= intval(date("H")) == intval("09") ? "selected" : "" ?> >09</option>
                                                <option value="10" <?= intval(date("H")) == intval("10") ? "selected" : "" ?> >10</option>
                                                <option value="11" <?= intval(date("H")) == intval("11") ? "selected" : "" ?> >11</option>
                                                <option value="12" <?= intval(date("H")) == intval("12") ? "selected" : "" ?> >12</option>
                                                <option value="13" <?= intval(date("H")) == intval("13") ? "selected" : "" ?> >13</option>
                                                <option value="14" <?= intval(date("H")) == intval("14") ? "selected" : "" ?> >14</option>
                                                <option value="15" <?= intval(date("H")) == intval("15") ? "selected" : "" ?> >15</option>
                                                <option value="16" <?= intval(date("H")) == intval("16") ? "selected" : "" ?> >16</option>
                                                <option value="17" <?= intval(date("H")) == intval("17") ? "selected" : "" ?> >17</option>
                                                <option value="18" <?= intval(date("H")) == intval("18") ? "selected" : "" ?> >18</option>
                                                <option value="19" <?= intval(date("H")) == intval("19") ? "selected" : "" ?> >19</option>
                                                <option value="20" <?= intval(date("H")) == intval("20") ? "selected" : "" ?> >20</option>
                                                <option value="21" <?= intval(date("H")) == intval("21") ? "selected" : "" ?> >21</option>
                                                <option value="22" <?= intval(date("H")) == intval("22") ? "selected" : "" ?> >22</option>
                                                <option value="23" <?= intval(date("H")) == intval("23") ? "selected" : "" ?> >23</option>
                                                <option value="24" <?= intval(date("H")) == intval("24") ? "selected" : "" ?> >24</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-4 col-md-2">
                                            <select   required="required" name="tgl_i" class="form-control inputKonfirmasiMenit">
                                                <option value="00" <?= intval(date("i")) == intval("01") ? "selected" : "" ?> >00</option>
                                                <option value="01" <?= intval(date("i")) == intval("01") ? "selected" : "" ?> >01</option>
                                                <option value="02" <?= intval(date("i")) == intval("02") ? "selected" : "" ?> >02</option>
                                                <option value="03" <?= intval(date("i")) == intval("03") ? "selected" : "" ?> >03</option>
                                                <option value="04" <?= intval(date("i")) == intval("04") ? "selected" : "" ?> >04</option>
                                                <option value="05" <?= intval(date("i")) == intval("05") ? "selected" : "" ?> >05</option>
                                                <option value="06" <?= intval(date("i")) == intval("06") ? "selected" : "" ?> >06</option>
                                                <option value="07" <?= intval(date("i")) == intval("07") ? "selected" : "" ?> >07</option>
                                                <option value="08" <?= intval(date("i")) == intval("08") ? "selected" : "" ?> >08</option>
                                                <option value="09" <?= intval(date("i")) == intval("09") ? "selected" : "" ?> >09</option>
                                                <option value="10" <?= intval(date("i")) == intval("10") ? "selected" : "" ?> >10</option>
                                                <option value="11" <?= intval(date("i")) == intval("11") ? "selected" : "" ?> >11</option>
                                                <option value="12" <?= intval(date("i")) == intval("12") ? "selected" : "" ?> >12</option>
                                                <option value="13" <?= intval(date("i")) == intval("13") ? "selected" : "" ?> >13</option>
                                                <option value="14" <?= intval(date("i")) == intval("14") ? "selected" : "" ?> >14</option>
                                                <option value="15" <?= intval(date("i")) == intval("15") ? "selected" : "" ?> >15</option>
                                                <option value="16" <?= intval(date("i")) == intval("16") ? "selected" : "" ?> >16</option>
                                                <option value="17" <?= intval(date("i")) == intval("17") ? "selected" : "" ?> >17</option>
                                                <option value="18" <?= intval(date("i")) == intval("18") ? "selected" : "" ?> >18</option>
                                                <option value="19" <?= intval(date("i")) == intval("19") ? "selected" : "" ?> >19</option>
                                                <option value="20" <?= intval(date("i")) == intval("20") ? "selected" : "" ?> >20</option>
                                                <option value="21" <?= intval(date("i")) == intval("21") ? "selected" : "" ?> >21</option>
                                                <option value="22" <?= intval(date("i")) == intval("22") ? "selected" : "" ?> >22</option>
                                                <option value="23" <?= intval(date("i")) == intval("23") ? "selected" : "" ?> >23</option>
                                                <option value="24" <?= intval(date("i")) == intval("24") ? "selected" : "" ?> >24</option>
                                                <option value="25" <?= intval(date("i")) == intval("25") ? "selected" : "" ?> >25</option>
                                                <option value="26" <?= intval(date("i")) == intval("26") ? "selected" : "" ?> >26</option>
                                                <option value="27" <?= intval(date("i")) == intval("27") ? "selected" : "" ?> >27</option>
                                                <option value="28" <?= intval(date("i")) == intval("28") ? "selected" : "" ?> >28</option>
                                                <option value="29" <?= intval(date("i")) == intval("29") ? "selected" : "" ?> >29</option>
                                                <option value="30" <?= intval(date("i")) == intval("30") ? "selected" : "" ?> >30</option>
                                                <option value="31" <?= intval(date("i")) == intval("31") ? "selected" : "" ?> >31</option>
                                                <option value="32" <?= intval(date("i")) == intval("32") ? "selected" : "" ?> >32</option>
                                                <option value="33" <?= intval(date("i")) == intval("33") ? "selected" : "" ?> >33</option>
                                                <option value="34" <?= intval(date("i")) == intval("34") ? "selected" : "" ?> >34</option>
                                                <option value="35" <?= intval(date("i")) == intval("35") ? "selected" : "" ?> >35</option>
                                                <option value="36" <?= intval(date("i")) == intval("36") ? "selected" : "" ?> >36</option>
                                                <option value="37" <?= intval(date("i")) == intval("37") ? "selected" : "" ?> >37</option>
                                                <option value="38" <?= intval(date("i")) == intval("38") ? "selected" : "" ?> >38</option>
                                                <option value="39" <?= intval(date("i")) == intval("39") ? "selected" : "" ?> >39</option>
                                                <option value="40" <?= intval(date("i")) == intval("40") ? "selected" : "" ?> >40</option>
                                                <option value="41" <?= intval(date("i")) == intval("41") ? "selected" : "" ?> >41</option>
                                                <option value="42" <?= intval(date("i")) == intval("42") ? "selected" : "" ?> >42</option>
                                                <option value="43" <?= intval(date("i")) == intval("43") ? "selected" : "" ?> >43</option>
                                                <option value="44" <?= intval(date("i")) == intval("44") ? "selected" : "" ?> >44</option>
                                                <option value="45" <?= intval(date("i")) == intval("45") ? "selected" : "" ?> >45</option>
                                                <option value="46" <?= intval(date("i")) == intval("46") ? "selected" : "" ?> >46</option>
                                                <option value="47" <?= intval(date("i")) == intval("47") ? "selected" : "" ?> >47</option>
                                                <option value="48" <?= intval(date("i")) == intval("48") ? "selected" : "" ?> >48</option>
                                                <option value="49" <?= intval(date("i")) == intval("49") ? "selected" : "" ?> >49</option>
                                                <option value="50" <?= intval(date("i")) == intval("50") ? "selected" : "" ?> >50</option>
                                                <option value="51" <?= intval(date("i")) == intval("51") ? "selected" : "" ?> >51</option>
                                                <option value="52" <?= intval(date("i")) == intval("52") ? "selected" : "" ?> >52</option>
                                                <option value="53" <?= intval(date("i")) == intval("53") ? "selected" : "" ?> >53</option>
                                                <option value="54" <?= intval(date("i")) == intval("54") ? "selected" : "" ?> >54</option>
                                                <option value="55" <?= intval(date("i")) == intval("55") ? "selected" : "" ?> >55</option>
                                                <option value="56" <?= intval(date("i")) == intval("56") ? "selected" : "" ?> >56</option>
                                                <option value="57" <?= intval(date("i")) == intval("57") ? "selected" : "" ?> >57</option>
                                                <option value="58" <?= intval(date("i")) == intval("58") ? "selected" : "" ?> >58</option>
                                                <option value="59" <?= intval(date("i")) == intval("59") ? "selected" : "" ?> >59</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>Catatan <span class="labelKonfirmasi">Konfirmasi</span></td>
                            <td class="tdKonfirmasiNote">
                                <input type="hidden" name="id" value="" class="inputKonfirmasiId"/>
                                <input type="hidden" name="proses" value="" class="inputKonfirmasiProses"/>
                                <textarea name="note" class="form-control inputKonfirmasiNote" max="1000" rows="4"></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="tableKonfirmasiDetail" style="display: none;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Donasi</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btnCloseKonfirmasiSedekah" data-bs-dismiss="modal">Tutup</button>
                <input type="submit" class="btn btn-primary" value="Simpan"/>
            </div>
         </div>
        </div>
    </div>
</form>