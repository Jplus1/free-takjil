<?php
/**
 * Radiate functions and definitions
 *
 * @package    ThemeGrill
 * @subpackage Radiate
 * @since      Radiate 1.0
 */
require_once "versiResource.php";

require_once "Util.php";
use radiate\Util;
Util::sessionStart();	

add_action('init', 'StartSession', 1);
function StartSession() {
    // if(!session_id()) {
    //     session_start();
    // }
    Util::sessionStart();	
}

// function add_my_custom_route_to_url( $rules ) {
//     $new_rules = [];
//     $iso_codes = '(?:us/|uk/|donasi/)?';

//     // home page
//     $new_rules[$iso_codes . '/?$'] = 'index.php';

//     // Change other rules
//     foreach ( $rules as $key => $rule ) {
//         if ( substr( $key, 0, 1 ) === '^' ) { 
//             $new_rules[ $iso_codes . substr( $key, 1 ) ] = $rule;
//         } else {
//             $new_rules[ $iso_codes . $key ] = $rule;
//         }
//     }

//     return $new_rules;
// }
// add_filter( 'rewrite_rules_array', 'add_my_custom_route_to_url' );
// flush_rewrite_rules();

add_action( 'init',  function() {
    add_rewrite_rule( 'donasi/([a-z0-9-]+)[/]?$', 'index.php?myparamname=$matches[1]', 'top' );
} );

add_filter( 'query_vars', function( $query_vars ) {
	// $query_vars[] = $key;
	$query_vars[] = 'myparamname';
    
	foreach($_GET as $key=>$value) {
		$query_vars[] = $key;
	}
    return $query_vars;
} );

add_action('template_include', function( $template ) {
	// echo "<pre>";
	// print_r($_REQUEST);
	// echo "</pre>";
	// die("INI APA: $template");

	$q = Util::getRequest("q");
	$listQ = explode("/", $q);
	if($listQ && $listQ[0] == "donasi") {
		$template =  get_template_directory() . '/donasi-page.php'; //default

		$count = count($listQ);
		if($count > 1) {
			switch($q) {
				case "donasi/per/tgl":
					$template =  get_template_directory() . '/donasi-per-tgl-page.php'; //default
					break;
				case "donasi/all/daily":
					$template =  get_template_directory() . '/donasi-all-daily.php'; //default
					break;
			}
		}
	}

	return $template;
} );
flush_rewrite_rules();

// function custom_rewrite_rule() {
// 	add_rewrite_rule('admin/([^/]+)/([^/]+)/?$',
// 	  'index.php?controller=$matches[1]&method=$matches[2]',
// 	  'top');
// }  
// add_action('init', 'custom_rewrite_rule', 10, 0);

// function donasi() {
//     echo "<pre>";
// 	print_r($_REQUEST);
//     echo "</pre>";
// }
// add_action('parse_request', 'donasi', 0);
// flush_rewrite_rules();

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 768; /* pixels */
}

if ( ! function_exists( 'radiate_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function radiate_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on radiate, use a find and replace
		 * to change 'radiate' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'radiate', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 * Post thumbail is used for pages that are shown in the featured section of Front page.
		 */
		add_theme_support( 'post-thumbnails' );

		// Gutenberg wide layout support.
		add_theme_support( 'align-wide' );

		// Gutenberg block layout support.
		add_theme_support( 'wp-block-styles' );

		// Gutenberg editor support.
		add_theme_support( 'responsive-embeds' );

		// Supporting title tag via add_theme_support (since WordPress 4.1)
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => __( 'Primary Menu', 'radiate' ),
			)
		);

		// Enable support for Post Formats.
		add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

		// Setup the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'radiate_custom_background_args',
				array(
					'default-color' => 'EAEAEA',
					'default-image' => '',
				)
			)
		);

		// Adding excerpt option box for pages as well
		add_post_type_support( 'page', 'excerpt' );

		// Cropping images to different sizes to be used in the theme
		add_image_size( 'featured-image-medium', 768, 350, true );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		// Gutenberg wide layout support.
		add_theme_support( 'align-wide' );

		// Gutenberg block styles support.
		add_theme_support( 'wp-block-styles' );

		// Gutenberg responsive embeds support.
		add_theme_support( 'responsive-embeds' );

		// Enable support for WooCommerce
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );
	}
endif; // radiate_setup
add_action( 'after_setup_theme', 'radiate_setup' );

/**
 * Register widgetized area and update sidebar with default widgets.
 */
function radiate_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Sidebar', 'radiate' ),
			'id'            => 'sidebar-1',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		)
	);
}

add_action( 'widgets_init', 'radiate_widgets_init' );

/**
 * Assign the Radiate version to a variable.
 */
$radiate_theme = wp_get_theme( 'radiate' );

define( 'RADIATE_THEME_VERSION', $radiate_theme->get( 'Version' ) );

/**
 * Enqueue scripts and styles.
 */
function radiate_scripts() {
	// Load our main stylesheet.
	wp_enqueue_style( 'radiate-css-bootstrap', get_home_url() . '/static/css/bootstrap4/bootstrap.min.css', null, 'v4.0.0');
	wp_enqueue_style( 'radiate-js-bootstrap', get_home_url() . '/static/css/bootstrap4/bootstrap.bundle.min.js', null, 'v4.0.0');

	// Load our main stylesheet.
	wp_enqueue_style( 'radiate-css-fontawesome5', get_home_url() . '/static/css/fontawesome5/css/all.min.css', null, 'v4.0.0');
	wp_enqueue_style( 'radiate-js-fontawesome5', get_home_url() . '/static/css/fontawesome5/js/all.min.js', null, 'v4.0.0');

//    wp_enqueue_script( 'bootstrap.bundle.min.js', get_home_url() . '/static/css/bootstrap.bundle.min.js', ('jquery'));

	$versi = VERSI_RESOURCE;

	wp_enqueue_style( 'radiate-style', get_stylesheet_uri(), null, '1.02');
	wp_enqueue_style( 'radiate-css-custom', get_home_url() . '/static/css/custom.css', null, $versi);
	wp_enqueue_script( 'JQCustom-js', get_home_url() . '/static/js/JQCustom.js', null, $versi);
	wp_enqueue_script( 'JsPDF-js', get_home_url() . '/static/js/jspdf.umd.min.js', null, $versi);
	// wp_enqueue_script( 'JsPDF-js', get_home_url() . '/static/js/jspdf.debug.js', null, $versi);


	wp_enqueue_style( 'radiate-google-fonts', '//fonts.googleapis.com/css?family=Roboto|Merriweather:400,300' );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'radiate-genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.3.1' );

	wp_enqueue_script( 'radiate-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'radiate-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script( 'radiate-custom-js', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), false, true );

	$radiate_header_image_link = get_header_image();
	wp_localize_script( 'radiate-custom-js', 'radiateScriptParam', array( 'radiate_image_link' => $radiate_header_image_link ) );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'html5shiv', get_template_directory_uri() . '/js/html5shiv.js', array(), '3.7.3', false );
	wp_script_add_data( 'html5shiv', 'conditional', 'lte IE 8' );

}

add_action( 'wp_enqueue_scripts', 'radiate_scripts' );

/**
 * Enqueue Google fonts and editor styles.
 */
function radiate_block_editor_styles() {
	wp_enqueue_style( 'radiate-editor-googlefonts', '//fonts.googleapis.com/css2?family=Roboto|Merriweather:400,300' );
	wp_enqueue_style( 'radiate-block-editor-styles', get_template_directory_uri() . '/style-editor-block.css' );
}

add_action( 'enqueue_block_editor_assets', 'radiate_block_editor_styles', 1, 1 );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Calling in the admin area for the Welcome Page as well as for the new theme notice too.
 */
if ( is_admin() ) {
	require get_template_directory() . '/inc/admin/class-radiate-admin.php';
	require get_template_directory() . '/inc/admin/class-radiate-dashboard.php';
	require get_template_directory() . '/inc/admin/class-radiate-notice.php';
	require get_template_directory() . '/inc/admin/class-radiate-welcome-notice.php';
	require get_template_directory() . '/inc/admin/class-radiate-upgrade-notice.php';
	require get_template_directory() . '/inc/admin/class-radiate-theme-review-notice.php';
}

require_once "apiSedekah.php";