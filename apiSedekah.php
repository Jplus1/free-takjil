<?php 
require_once "Util.php";
use radiate\Util;

function sedekahIndex( WP_REST_Request $r ) {
	global $wpdb;

	$metakey = "jumlah_target_takjil_harian";
	$dbquery = $wpdb->prepare("SELECT meta_value FROM " . $wpdb->prefix . "usermeta WHERE meta_key = %s", $metakey);
	$jumlah_target_takjil_harian = $wpdb->get_var($dbquery);
	$jumlah_target_takjil_harian = intval($jumlah_target_takjil_harian);

	// $hariToday = 14;
	$hariToday = 0;

	$hariTutup = $hariToday + 1;
	$dbquery = $wpdb->prepare("
		SELECT a.*
		, CURRENT_DATE() + interval $hariTutup day as tgl_tutup
		, case when (CURRENT_DATE() + interval $hariTutup day) >= a.tgl then 1 else 0 end as is_tgl_tutup 
		, case when (CURRENT_DATE() + interval $hariToday day) = a.tgl then 1 else 0 end as is_hari_ini 
		, CURRENT_DATE() + interval $hariToday day as tgl_hari_ini  
		from m_tgl a 
		order by a.tgl");
	$allTgl = $wpdb->get_results($dbquery);

	$result = Util::defaultResult();
	$result["jumlah_target_takjil_harian"] = $jumlah_target_takjil_harian;
	$result["data"] = $allTgl;
	return $result;
}

function sedekahMine( WP_REST_Request $r ) {
	global $wpdb;

	$donaturId = Util::getSession("donaturId");

	$metakey = "jumlah_target_takjil_harian";
	$dbquery = $wpdb->prepare("SELECT meta_value FROM " . $wpdb->prefix . "usermeta WHERE meta_key = %s", $metakey);
	$jumlah_target_takjil_harian = $wpdb->get_var($dbquery);
	$jumlah_target_takjil_harian = intval($jumlah_target_takjil_harian);

	$sql = "
		SELECT b.tgl  as tgl2, a.* from m_tgl a 
		left join (select distinct tgl from m_donasi where donatur_id = $donaturId) b on b.tgl = a.tgl 
		order by a.tgl
		";


	$sql = "
		SELECT a.* from m_tgl a 
		where a.tgl in (select distinct tgl from m_donasi where donatur_id = $donaturId)  
		order by a.tgl
		";

		
	$dbquery = $wpdb->prepare($sql);
	$allTgl = $wpdb->get_results($dbquery);

	$result = Util::defaultResult();
	$result["jumlah_target_takjil_harian"] = $jumlah_target_takjil_harian;
	$result["data"] = $allTgl;
	return $result;
}

function sedekahKonfirmasi( WP_REST_Request $r ) {
	global $wpdb;

	$tgl_d = Util::getPost("tgl_d");
	$tgl_m = Util::getPost("tgl_m");
	$tgl_y = Util::getPost("tgl_y");
	$tgl_h = Util::getPost("tgl_h");
	$tgl_i = Util::getPost("tgl_i");
	$id = Util::getPost("id");
	// $tgl = Util::getPost("tgld");
	// $jenis_donasi = Util::getPost("jenis_donasid");
	// $donatur_id = Util::getPost("donatur_idd");
	
	$note = Util::getPost("note");
	$proses = Util::getPost("proses");

	$tgl = "$tgl_y-$tgl_m-$tgl_d $tgl_h:$tgl_i";
	$isRelawan = Util::getSession("isRelawan");
	$userId = Util::getSession("donaturId");

	if(!$userId) {
		$result = Util::defaultResult(false, "Maaf, Anda harus login untuk bisa memproses ini.");
		$result["isRelawan"] = $isRelawan;
		$result["userId"] = $userId;
		return $result;
	}

	if(!$isRelawan) {
		$result = Util::defaultResult(false, "Maaf, hanya Relawan yang bisa memproses ini. Coba logout dan login kembali.");
		$result["isRelawan"] = $isRelawan;
		$result["userId"] = $userId;
		return $result;
	}

	$table = 'm_donasi';
	if($proses == "diterima") {
		$data = array(
			'diterima_at' => $tgl,
			'diterima_note' => $note,
			'diterima_by' => $userId,
		);
	} else {
		//proses default = konfirmasi
		$data = array(
			'konfirmasi_at' => $tgl,
			'konfirmasi_note' => $note,
			'konfirmasi_by' => $userId,
		);
	}
	// $where = [
	// 	"tgl"=>$tgl,
	// 	"jenis_donasi"=>$jenis_donasi,
	// 	"donatur_id"=>$donatur_id,
	// ];
	$where = [
		"id"=>$id,
	];
	$ok = $wpdb->update( $table, $data, $where);
	// $ok = $wpdb->update(tableName, columns, condition, params);

	$result = Util::defaultResult();
	$result["table"] = $table;
	$result["data"] = $data;
	$result["where"] = $where;
	$result["ok"] = $ok;
	return $result;
}


function sedekahSave( WP_REST_Request $r ) {
	global $wpdb;

	// $result = Util::defaultResult();
	// $result["POST"] = $_POST;
	// return $result;

    $inputJenisDonasi = Util::getPostInt("inputJenisDonasi");//1 = makanan, 2 = uang 
    $inputTanggal = Util::getPost("tanggal");
    
	$inputJumlah_tipe_1 = Util::getPostInt("jumlah", 0);
	$inputJumlah_tipe_2 = Util::getPostInt("inputTotalJumlahMakanan", 0);

	$inputJumlah = $inputJenisDonasi == 1 ? $inputJumlah_tipe_1 : $inputJumlah_tipe_2;

    $inputTotalJumlahUang = Util::getPostInt("inputTotalJumlahUang", 0);
    $inputDonaturNama = Util::getPost("nama");
    $inputDonaturNamaSamaran = Util::getPost("namaSamaran");
    $inputDonaturTelp = Util::getPost("telp");
    $inputDonaturAlamat = Util::getPost("alamat");
    $inputDonaturId = Util::getPost("inputDonaturId");


    $inputDonaturIdLama = 0;
    $sqlCariDonaturLama = "";
    if(!$inputDonaturId) {
		$donaturIds = $wpdb->get_col( $wpdb->prepare( "SELECT id FROM m_donatur WHERE telp = %s", $inputDonaturTelp ) );
		if($donaturIds) {
			$inputDonaturId = $donaturIds[0];
			$inputDonaturIdLama = $inputDonaturId;
		}
		
		//user belum terdaftar, create baru
		if($inputDonaturId) {
			$table = 'm_donatur';
			$data = array(
				'nama' => $inputDonaturNama,
				'nama_samaran' => $inputDonaturNamaSamaran,
				'telp' => $inputDonaturTelp,
				'alamat' => $inputDonaturAlamat,
			);
			$format = array('%s','%s','%s','%s');
			$wpdb->update( $table, $data, array( 'id' => $inputDonaturId ) );
		} else {
			$table = 'm_donatur';
			$data = array(
				'nama' => $inputDonaturNama,
				'nama_samaran' => $inputDonaturNamaSamaran,
				'telp' => $inputDonaturTelp,
				'alamat' => $inputDonaturAlamat,
			);
			$format = array('%s','%s','%s','%s');
			$wpdb->insert($table,$data,$format);
			$inputDonaturId = $wpdb->insert_id;
		}
    }

	// //simpan session dulu
    Util::setSession("donaturId", $inputDonaturId);
    Util::setSession("donaturNama", $inputDonaturNama);
    Util::setSession("donaturNamaSamaran", $inputDonaturNamaSamaran);
    Util::setSession("donaturTelp", $inputDonaturTelp);
    Util::setSession("donaturAlamat", $inputDonaturAlamat);

	$dataDonatur = [];
	$dataDonatur["id"] = $inputDonaturId;
	$dataDonatur["nama"] = $inputDonaturNama;
	$dataDonatur["nama_samaran"] = $inputDonaturNamaSamaran;
	$dataDonatur["telp"] = $inputDonaturTelp;
	$dataDonatur["alamat"] = $inputDonaturAlamat;

	$table = 'm_donasi';
	$whereDonasi = [
		$inputTanggal,
		$inputJenisDonasi,
		$inputDonaturId,
	];
	$steps = [];
	$oldRec = $wpdb->get_row( $wpdb->prepare( 
			"SELECT id , jumlah_pesan, jumlah_porsi 
			FROM m_donasi  
			WHERE tgl = %s
			and jenis_donasi = %d
			and donatur_id = %d
			", $whereDonasi ) );
	if ( is_object( $oldRec ) ) {
		$inputDonasiId = $oldRec->id;
		$oldJumlahPesan = $oldRec->jumlah_pesan;
		$oldJumlahPorsi = $oldRec->jumlah_porsi;
		
		if($inputJenisDonasi == 2) {
			$jumlah_pesan = $oldJumlahPesan + $inputTotalJumlahUang;
		} else {
			$jumlah_pesan = $oldJumlahPesan + $inputJumlah;
		}

		$jumlah_porsi = $oldJumlahPorsi + $inputJumlah;
		$data = array(
			'donatur_nama' => $inputDonaturNamaSamaran,
			'jumlah_pesan' => $jumlah_pesan,
			'jumlah_porsi' => $jumlah_porsi,
		);
		$format = array('%d','%d','%s','%s','%d','%d');
		$steps[] = __LINE__ . " | update donasiID: $inputDonasiId | jumlah_pesan=$jumlah_pesan | jumlah_porsi: $jumlah_porsi";
		$resultUpdate = $wpdb->update($table,$data,["id"=>$inputDonasiId]);
		$steps[] = __LINE__ . " | result update";
		$steps[] = $resultUpdate;
	} else {
		$steps[] = __LINE__ . " insert new";
		if($inputJenisDonasi == 2) {
			$jumlah_pesan =  $inputTotalJumlahUang;
		} else {
			$jumlah_pesan =  $inputJumlah;
		}

		$data = array(
			'jenis_donasi' => $inputJenisDonasi,
			'donatur_id' => $inputDonaturId,
			'donatur_nama' => $inputDonaturNamaSamaran,
			'tgl' => $inputTanggal,
			'jumlah_pesan' => $jumlah_pesan,
			'jumlah_porsi' => $inputJumlah,
		);
		$format = array('%d','%d','%s','%s','%d','%d');
		$resultInsert = $wpdb->insert($table,$data,$format);
		$inputDonasiId = $wpdb->insert_id;
		$steps[] = __LINE__ . " | result insert";
		$steps[] = $resultInsert;
	}

	if($inputJenisDonasi == 2) {//uang
		$listIdPaket = Util::getPost("idPaket");
		$listHargaPaket = Util::getPost("hargaPaket");
		$listJumlahPaket = Util::getPost("jumlahPaket");
		for($xx=0; $xx < count($listIdPaket); $xx++) {
			$idPaket = $listIdPaket[$xx];
			$hargaPaket = $listHargaPaket[$xx]; //gak dipake
			$jumlahPaket = intval($listJumlahPaket[$xx]);

			if(!$jumlahPaket) {
				continue;
			}

			$table = 'm_donasi_paket';
			$whereDonasiPaket = [
				$inputDonasiId,
				$idPaket,
			];
			$oldRecPaket = $wpdb->get_row( $wpdb->prepare( 
				"SELECT id , jumlah_pesan
				FROM m_donasi_paket
				WHERE donasi_id = %d
				and paket_id = %d
				", $whereDonasiPaket ) );
			$jumlah_pesanLama = 0;
			if ( is_object( $oldRecPaket ) ) { 
				$idDonasiPaket = $oldRecPaket->id;
				$jumlah_pesanLama = $oldRecPaket->jumlah_pesan;

				$jumlahPesanBaru = $jumlah_pesanLama + $jumlahPaket;
				$data = array(
					'donasi_id' => $inputDonasiId,
					'paket_id' => $idPaket,
					'jumlah_pesan' => $jumlahPesanBaru,
				);
				$format = array('%d','%d','%d',);
				$wpdb->update($table,$data,["id"=>$idDonasiPaket]);
			} else {
				$jumlahPesanBaru = $jumlah_pesanLama + $jumlahPaket;
				$data = array(
					'donasi_id' => $inputDonasiId,
					'paket_id' => $idPaket,
					'jumlah_pesan' => $jumlahPesanBaru,
				);
				$format = array('%d','%d','%d',);
				$wpdb->insert($table,$data,$format);
			}
		}
	}
	$resutlUpdateSedekah = updateTableTgl();

	$kode_akses = "";
	$rowDonatur = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM m_donatur WHERE id = %d", $inputDonaturId ) );
	if($rowDonatur) {
		$kode_akses = $rowDonatur->kode_akses;
	}



	$status = true;
	$msg = "";
    $data = [];
    $data["inputDonasiId"] = $inputDonasiId;
    $data["inputDonaturId"] = $inputDonaturId;
    $data["inputDonaturIdLama"] = $inputDonaturIdLama;
    $data["donaturKodeAkses"] = $kode_akses;
    $data["donatur"] = $dataDonatur;
    $data["resutlUpdateSedekah"] = $resutlUpdateSedekah;
    $data["resultUpdate"] = $resultUpdate;
    $data["resultInsert"] = $resultInsert;
    $data["steps"] = $steps;

    $result = Util::defaultResult($status, $msg, $data);
    return $result;
}

function updateTableTgl() {
	global $wpdb;

	$sql = "
		update m_tgl a 
		left join (select jenis_donasi, sum(jumlah_porsi) as jumlah_porsi, tgl
		from m_donasi 
		where jenis_donasi=1 
		group by jenis_donasi, tgl) aa  on aa.tgl = a.tgl 
		left join (select jenis_donasi, sum(jumlah_porsi) as jumlah_porsi, tgl
		from m_donasi 
		where jenis_donasi=2 
		group by jenis_donasi, tgl) bb on bb.tgl = a.tgl 
		set 
		a.jumlah_porsi_sedekah_makanan = ifnull(aa.jumlah_porsi, 0)
		, a.jumlah_porsi_sedekah_uang = ifnull(bb.jumlah_porsi, 0)
		, a.jumlah_porsi_total_sedekah = ifnull(aa.jumlah_porsi, 0) + ifnull(bb.jumlah_porsi, 0)
		, a.jumlah_porsi_total_kosong = a.jumlah_porsi_total_target - (ifnull(aa.jumlah_porsi, 0) + ifnull(bb.jumlah_porsi, 0))
	";
	return $wpdb->query($sql);
}


function sedekahDeleteMine( WP_REST_Request $r ) {
	global $wpdb;

	$id = Util::getPost("id");
	$tgl = Util::getPost("tgl");
	if(!$id && !$tgl) {
		$result = Util::defaultResult(false, "ID / Tanggal mandatory");
		return $result;
	}

	$donaturId = Util::getSession("donaturId");
	if(!$donaturId) {
		$result = Util::defaultResult(false, "Anda harus login untuk menghapus data Anda.");
		return $result;
	}

	$isRelawan = Util::getSession("isRelawan");
	if($isRelawan) {
		if($id) {
			$sql = "
				delete from m_donasi_paket 
				where donasi_id in (
					select distinct id from m_donasi 
					where id=%d 
				)
			";
			$wpdb->query($wpdb->prepare($sql, $id)); //kenapa tidak pakai donaturId? relawan boleh

			$sql = "
				delete from m_donasi where id=%d
			";
			$wpdb->query($wpdb->prepare($sql, $id));//kenapa tidak pakai donaturId? relawan boleh
		}
		

		if($tgl) {
			$sql = "
				delete from m_donasi_paket 
				where donasi_id in (
					select distinct id from m_donasi 
					where tgl=%s 
				)
			";
			$wpdb->query($wpdb->prepare($sql, $tgl));//kenapa tidak pakai donaturId? relawan boleh

			$sql = "
				delete from m_donasi where tgl=%s 
			";
			$wpdb->query($wpdb->prepare($sql, $tgl));//kenapa tidak pakai donaturId? relawan boleh
		}
	} else {
		if($id) {
			$sql = "
				delete from m_donasi_paket 
				where donasi_id in (
					select distinct id from m_donasi 
					where id=%d and donatur_id=%d
				)
			";
			$wpdb->query($wpdb->prepare($sql, $id, $donaturId)); //kenapa harus pakai donaturId? biar gak di hack bisa hapus data orang lain

			$sql = "
				delete from m_donasi where id=%d and donatur_id=%d
			";
			$wpdb->query($wpdb->prepare($sql, $id, $donaturId));//kenapa harus pakai donaturId? biar gak di hack bisa hapus data orang lain
		}
		

		if($tgl) {
			$sql = "
				delete from m_donasi_paket 
				where donasi_id in (
					select distinct id from m_donasi 
					where tgl=%s and donatur_id=%d
				)
			";
			$wpdb->query($wpdb->prepare($sql, $tgl, $donaturId));//kenapa harus pakai donaturId? biar gak di hack bisa hapus data orang lain

			$sql = "
				delete from m_donasi where tgl=%s and donatur_id=%d
			";
			$wpdb->query($wpdb->prepare($sql, $tgl, $donaturId));//kenapa harus pakai donaturId? biar gak di hack bisa hapus data orang lain
		}		
	}
	$resutlUpdateSedekah = updateTableTgl();

	$result = Util::defaultResult();
	return $result;
}


function sedekahReset( WP_REST_Request $r ) {

	die("HATI-HATI!");
	
	global $wpdb;

	$sql = "
		delete from m_donasi_paket 
	";
	$wpdb->query($wpdb->prepare($sql));


	$sql = "
		delete from m_donasi
	";
	$wpdb->query($wpdb->prepare($sql));


	$sql = "
		delete from m_donatur
	";
	$wpdb->query($wpdb->prepare($sql));


	$sql = "
		update m_tgl
		set jumlah_porsi_sedekah_makanan=0,
		jumlah_porsi_sedekah_uang=0,
		jumlah_porsi_total_sedekah=0
	";
	$wpdb->query($wpdb->prepare($sql));

	$result = Util::defaultResult();
	return $result;
}

function sedekahGetByTanggal( WP_REST_Request $r ) {
	global $wpdb;
    // $tgl = Util::getPost("tgl");
    $tgl = Util::getRequest("tgl");

    if(!$tgl) {
    	$result = Util::defaultResult(false, "Tgl adalah mandatory");
    	$result["tgl"] = $tgl;
    	return $result;
    }

		// , if(ifnull(b.nama_samaran, %s) = %s, a.nama, a.nama_samaran) as donatur_nama 
	$rows = $wpdb->get_results( $wpdb->prepare( "
		SELECT 
		a.id
		,a.jenis_donasi 
		, IF(a.jenis_donasi = 1, 'Makanan', 'Uang') as jenis_donasi_nama 
		, sum(a.jumlah_porsi) as jumlah_porsi 
		, sum(a.jumlah_pesan) as jumlah_pesan
		, a.donatur_id
		, if(ifnull(b.nama_samaran, '') = '', b.nama, b.nama_samaran) as donatur_nama , b.telp as donatur_telp  
		, a.konfirmasi_at, a.diterima_at 
		, c.nama as konfirmasi_oleh
		, d.nama as diterima_oleh 
		FROM m_donasi a 
		join m_donatur b on b.id = a.donatur_id
		left outer join m_donatur c on c.id = a.konfirmasi_by
		left outer join m_donatur d on d.id = a.diterima_by
		WHERE a.tgl = %s
		group by a.jenis_donasi, a.donatur_id
		order by if(ifnull(b.nama_samaran, '') = '', b.nama, b.nama_samaran), a.jenis_donasi 
		", [$tgl] ));
		
	
	$result = Util::defaultResult();
	$result["data"] = $rows;
	return $result;
}


function sedekahSummaryPerTgl( WP_REST_Request $r ) {
	global $wpdb;
    // $tgl = Util::getPost("tgl");
    $tgl = Util::getRequest("tgl");

    if(!$tgl) {
    	$result = Util::defaultResult(false, "Tgl adalah mandatory");
    	$result["tgl"] = $tgl;
    	return $result;
    }

	$rows = $wpdb->get_results( $wpdb->prepare( "
		SELECT 
		a.tgl
		, sum(a.jumlah_porsi) as jumlah_porsi 
		, sum(IF(a.jenis_donasi = 1, 0, a.jumlah_pesan)) as jumlah_rupiah 
		FROM m_donasi a 
		WHERE a.tgl = %s
		group by a.tgl 
		order by a.tgl
		", [$tgl] ));
		
	$result = Util::defaultResult();
	$result["data"] = $rows;
	return $result;
}

function sedekahAllDaily( WP_REST_Request $r ) {
	global $wpdb;

	$dbquery = $wpdb->prepare("SELECT a.* from m_paket a  order by a.nama");
	$allPaket = $wpdb->get_results($dbquery);

	$dbquery = $wpdb->prepare("SELECT a.* from m_tgl a  order by a.tgl");
	$allTgl = $wpdb->get_results($dbquery);

	//tgl | sum_jumlah_porsi | sum_jumlh_pesan | 
	$data = [];
	foreach($allTgl as $oneTgl) {
		$tgl = $oneTgl->tgl;
		$tglDMY = date("d-m-Y", strtotime($tgl));

		$one = [];
		$one["tgl"] = $tgl;
		$one["tgl_dmy"] = $tglDMY;
		$one["jumlah_porsi"] = "";
		$one["jumlah_rupiah"] = "";
		$one["jumlah_non_paket"] = "";

		foreach($allPaket as $paket) {
			$paketId = $paket->id;
			$key = "paket_" . $paketId;
			$one[$key] = "";
		}

		$data[$tgl] = $one;
	}
	
	$sumTgl = $wpdb->get_results( $wpdb->prepare( "
	SELECT 
	a.tgl
	, sum(b.jumlah_porsi) as jumlah_porsi 
	, sum(IF(b.jenis_donasi = 1, 0, b.jumlah_pesan)) as jumlah_rupiah 
	FROM m_tgl a 
	join m_donasi b on b.tgl = a.tgl  
	group by a.tgl 
	order by a.tgl
	"));
	foreach($sumTgl as $header) {
		$tgl = $header->tgl;
		$one = $data[$tgl];

		$one["tgl"] = $tgl;
		$one["jumlah_porsi"] = $header->jumlah_porsi;
		$one["jumlah_rupiah"] = $header->jumlah_rupiah;

		$data[$tgl] = $one;
	}

	$detailTgl = $wpdb->get_results( $wpdb->prepare( "
		SELECT 
		a.tgl
		, sum(c.jumlah_pesan) as jumlah_paket 
		, c.paket_id 
		FROM m_tgl a 
		join m_donasi b on b.tgl = a.tgl  
		join m_donasi_paket c on c.donasi_id = b.id 
		group by a.tgl, c.paket_id 
		order by a.tgl, c.paket_id 
	"));
	// print_r($detailTgl);
	// die("DIE");
	foreach($detailTgl as $detail) {
		$tgl = $detail->tgl;
		$one = $data[$tgl];
		$paketId = $detail->paket_id;
		$key = "paket_" . $paketId;
		$one[$key] = $detail->jumlah_paket;

		$data[$tgl] = $one;
	}


	$nonPaket = $wpdb->get_results( $wpdb->prepare( "
		SELECT 
		a.tgl
		, sum(b.jumlah_pesan) as jumlah_paket 
		FROM m_tgl a 
		join m_donasi b on b.tgl = a.tgl  
		left join m_donasi_paket c on c.donasi_id = b.id 
		where c.id is null 
		group by a.tgl
		order by a.tgl 
	"));
	// print_r($detailTgl);
	// die("DIE");
	foreach($nonPaket as $detail) {
		$tgl = $detail->tgl;
		$one = $data[$tgl];
		$paketId = $detail->paket_id;
		$key = "jumlah_non_paket";
		$one[$key] = $detail->jumlah_paket;

		$data[$tgl] = $one;
	}

	$newData = [];
	foreach($data as $key=>$value) {
		$newData[] = $value;
	}


	$result = Util::defaultResult();
	$result["data"] = $newData;
	return $result;
}


function sedekahGetAll1( WP_REST_Request $r ) {
	global $wpdb;

	$isRelawan = Util::getSessionInt("isRelawan");
	if(!$isRelawan) {
		$result = Util::defaultResult(false, "Maaf, hanya Admin yang boleh mengakses ini.");
		return $result;
	}

		// , if(ifnull(b.nama_samaran, %s) = %s, a.nama, a.nama_samaran) as donatur_nama 
	$rows = $wpdb->get_results( $wpdb->prepare( "
	select 
	a.tgl
	, c.nama as donasi_oleh, c.telp as donatur_telp, c.alamat as donatur_alamat 
	, case when a.jenis_donasi = 1 then 'Makanan' else 'Uang' end as jenis_donasi_nama
	, a.jumlah_pesan as jumlah_rupiah, a.jumlah_porsi
	, a.konfirmasi_at
	, d.nama as konfirmasi_oleh 
	, a.konfirmasi_note
	, a.diterima_at
	, e.nama as diterima_oleh  
	, a.diterima_note    	
	from m_donasi a   
	join m_donatur c on c.id = a.donatur_id 
	left join m_donatur d on d.id = a.konfirmasi_by 
	left join m_donatur e on e.id = a.diterima_by 
	order by a.tgl, c.nama, c.id, a.jenis_donasi
		"));
		
	$result = Util::defaultResult();
	$result["data"] = $rows;
	return $result;
}


function donaturGetAll1( WP_REST_Request $r ) {
	global $wpdb;

	$isRelawan = Util::getSessionInt("isRelawan");
	if(!$isRelawan) {
		$result = Util::defaultResult(false, "Maaf, hanya Admin yang boleh mengakses ini.");
		return $result;
	}

		// , if(ifnull(b.nama_samaran, %s) = %s, a.nama, a.nama_samaran) as donatur_nama 
	$rows = $wpdb->get_results( $wpdb->prepare( "
	select 
	#a.nama, a.telp, a.alamat 
	a.* 
	from m_donatur a
	order by (case when a.is_relawan is null then 0 else is_relawan end) desc, a.nama 
		"));
		
	$result = Util::defaultResult();
	$result["data"] = $rows;
	return $result;
}


function sedekahGetAll2( WP_REST_Request $r ) {
	global $wpdb;

	$isRelawan = Util::getSessionInt("isRelawan");
	if(!$isRelawan) {
		$result = Util::defaultResult(false, "Maaf, hanya Admin yang boleh mengakses ini.");
		return $result;
	}

		// , if(ifnull(b.nama_samaran, %s) = %s, a.nama, a.nama_samaran) as donatur_nama 
	$rows = $wpdb->get_results( $wpdb->prepare( "
		select 
		a.tgl
		, c.nama as donasi_oleh  , c.telp as donatur_telp  , c.alamat as donatur_alamat 
		, case when a.jenis_donasi = 1 then 'Makanan' else 'Uang' end as jenis_donasi_nama
		, a.jumlah_pesan as jumlah_rupiah, a.jumlah_porsi
		, a.konfirmasi_at
		, d.nama as konfirmasi_oleh 
		, a.konfirmasi_note
		, a.diterima_at
		, e.nama as diterima_oleh  
		, a.diterima_note    
		, b.nama as nama_paket, a2.jumlah_pesan as jumlah_pesan_paket, b.harga as harga_paket, (a2.jumlah_pesan * b.harga) as subtotal
		from m_donasi a   
		left join m_donasi_paket a2 on a2.donasi_id = a.id  
		left join m_paket b on b.id = a2.paket_id
		join m_donatur c on c.id = a.donatur_id 
		left join m_donatur d on d.id = a.konfirmasi_by 
		left join m_donatur e on e.id = a.diterima_by 
		order by a.tgl, c.nama, c.id, a.jenis_donasi, b.nama
		"));
		
	$result = Util::defaultResult();
	$result["data"] = $rows;
	return $result;
}


function sedekahGetById( WP_REST_Request $r ) {
	global $wpdb;
    $id = Util::getRequest("id");

    if(!$id) {
    	$result = Util::defaultResult(false, "ID adalah mandatory");
    	$result["id"] = $id;
    	return $result;
    }

		// , if(ifnull(b.nama_samaran, %s) = %s, a.nama, a.nama_samaran) as donatur_nama 
	$rows = $wpdb->get_results( $wpdb->prepare( "
		SELECT 
		a.id
		,a.jenis_donasi 
		, IF(a.jenis_donasi = 1, 'Makanan', 'Uang') as jenis_donasi_nama 
		, a.jumlah_porsi
		, a.jumlah_pesan
		, a.donatur_id
		, a.konfirmasi_at, a.diterima_at 
		, if(ifnull(b.nama_samaran, '') = '', b.nama, b.nama_samaran) as donatur_nama , b.telp as donatur_telp  
		, c.nama as konfirmasi_oleh
		, d.nama as diterima_oleh 
		FROM m_donasi a 
		join m_donatur b on b.id = a.donatur_id
		left outer join m_donatur c on c.id = a.konfirmasi_by
		left outer join m_donatur d on d.id = a.diterima_by
		WHERE a.id = %d
		", [$id] ));
	$oneRow = $rows ? $rows[0] : [];
	
		// , if(ifnull(b.nama_samaran, %s) = %s, a.nama, a.nama_samaran) as donatur_nama 
	$detail = $wpdb->get_results( $wpdb->prepare( "
			select 
			a.donasi_id, a.paket_id 
			,sum(a.jumlah_pesan) as jumlah_pesan, sum(a.jumlah_realisasi) as jumlah_realisasi 
			,b.nama, b.is_makanan_pokok, b.is_dihitung, b.url_media, b.is_active as paket_is_active 
			,b.harga
			from m_donasi_paket a 
			join m_paket b on b.id = a.paket_id 
			where a.donasi_id = %d  
			and a.is_active = 1 
			group by a.donasi_id, a.paket_id
			having sum(a.jumlah_pesan) > 0 
		", [$id] ));
		
	
	$result = Util::defaultResult();
	$result["data"] = $oneRow;
	$result["detail"] = $detail;
	return $result;
}


function sedekahTest( WP_REST_Request $request ) {
		global $wpdb;

		$inputDonaturTelp = "08121111111112";

		$sql = $wpdb->prepare("
			select id from m_donatur where telp = %s
		",
		array($inputDonaturTelp));
		$rowDonatur = $wpdb->get_row($sql);
		if($rowDonatur) {
			$inputDonaturId = $rowDonatur->id;
		} else {
			$inputDonaturId = null;
		}

		$result = Util::defaultResult(false, "testing jacky");
		$result["sql"] = $sql;
		$result["inputDonaturId"] = $inputDonaturId;
		return $result;		
}

function donaturLogin( WP_REST_Request $request ) {
	global $wpdb;

	$kodeAksesAsli = Util::getPost("kodeAkses");

	if(strpos($kodeAksesAsli, "#") !== false) {
		$kodeAkses = str_replace("#", "", $kodeAksesAsli);
	} else {
		$kodeAkses = "" . $kodeAksesAsli;
	}

	$fieldCaption = "Kode Akses";
	$fieldCaption = "No HP";
	if(!$kodeAkses) {
		$result = Util::defaultResult(false, "Maaf, $fieldCaption harus diisi.");		
		return $result;
	}

	$donatur = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM m_donatur WHERE telp = %s", $kodeAkses ) );
	if(!$donatur) {
		$result = Util::defaultResult(false, "Maaf, $fieldCaption Anda belum terdaftar sebagai donatur. Silakan melakukan donasi dahulu dengan memilih tanggal donasi yang tersisa kuota");
		return $result;
	}

	$isRelawan = $donatur->is_relawan;
	if($isRelawan) {
		if(strpos($kodeAksesAsli, "#") === false) {
			$result = Util::defaultResult(false, "Relawan memiliki login yang berbeda. Apakah Anda adalah pemilik akun ini?");
			return $result;
		}
	}

	// //simpan session dulu
    Util::setSession("donaturId", $donatur->id);
    Util::setSession("isRelawan", $donatur->is_relawan);
    Util::setSession("donatur", $donatur);
    Util::setSession("donaturNama", $donatur->nama);
    Util::setSession("donaturNamaSamaran", $donatur->nama_samaran);
    Util::setSession("donaturTelp", $donatur->telp);
    Util::setSession("donaturAlamat", $donatur->alamat);

	$result = Util::defaultResult();
	$result["data"] = $donatur;
	return $result;
};

function donaturSession( WP_REST_Request $request ) {
    $session = [];
    $session["donaturId"] = Util::getSession("donaturId", "");
	$session["isRelawan"] = Util::getSession("isRelawan", "");
    $session["donaturNama"] = Util::getSession("donaturNama", "");
    $session["donaturNamaSamaran"] = Util::getSession("donaturNamaSamaran", "");
    $session["donaturTelp"] = Util::getSession("donaturTelp", "");
    $session["donaturAlamat"] = Util::getSession("donaturAlamat", "");
    $session["donaturKodeAkses"] = Util::getSession("donaturKodeAkses", "");

	$status = true;
	$msg = "";

    $result = Util::defaultResult($status, $msg, $session);
	$result["session"] = $_SESSION;
    return $result;
}

function masterPaket( WP_REST_Request $request ) {
	global $wpdb;

	$tgl_is_spesial = Util::getRequest("tgl_is_spesial");

	$where_special = "";
	if($tgl_is_spesial) {
		$where_special = " and is_spesial=1 ";
	}

	$sql = "
		select * 
		from m_paket 
		where is_active = 1
		$where_special 
		order by is_makanan_pokok desc, nama 
	";
	$rows = $wpdb->get_results($sql);

	$makanan = [];
	$snack = [];
	foreach($rows as $one) {
		$is_makanan_pokok = Util::getArr($one, "is_makanan_pokok");
		if($is_makanan_pokok) {
			$makanan[] = $one;
		} else {
			$snack[] = $one;
		}
	}
	$data = [];
	$data["makanan"] = $makanan;
	$data["snack"] = $snack;
	// $data["sql"] = $sql;

	$status = true;
	$msg = "";
    $result = Util::defaultResult($status, $msg, $data);
    return $result;
}

function masterPaketNama( WP_REST_Request $request ) {
	global $wpdb;

	$sql = "
		select id, nama, is_makanan_pokok, is_dihitung 
		from m_paket 
		where is_active = 1
		order by is_makanan_pokok desc, nama 
	";
	$rows = $wpdb->get_results($sql);

	$status = true;
	$msg = "";
    $result = Util::defaultResult($status, $msg, $rows);
    return $result;
}

function profileOne( WP_REST_Request $request ) {
	global $wpdb;

	$sql = "
		select * 
		from m_donatur 
		where id = %d
	";

	$donaturId = Util::getGetInt("id");
	$donaturId or $donaturId = Util::getSession("donaturId");

	$dbquery = $wpdb->prepare($sql, $donaturId);
	$row = $wpdb->get_row($dbquery);

	$status = boolval($row);
	$msg = $status ? '' : 'You have to logged in to access this your profile';
    $result = Util::defaultResult($status, $msg, $row);
	$result["id"] = $donaturId;
	$result["data"] = $row;
    return $result;
}

function profileEdit( WP_REST_Request $request ) {
	global $wpdb;
	$donaturId = Util::getSession("donaturId");
	$nama = Util::getPost("nama");
	$nama_samaran = Util::getPost("nama_samaran");
	$telp = Util::getPost("telp");
	$alamat = Util::getPost("alamat");

	$sql = "
		update m_donatur
		set nama = %s, nama_samaran=%s, telp=%s, alamat=%s
		where id=%d
	";

	$dbquery = $wpdb->prepare($sql, $nama, $nama_samaran, $telp, $alamat, $donaturId);
	$ok = $wpdb->query($dbquery);

	$status = boolval($ok);
	$msg = $status ? '' : 'Data gagal disimpan. Silahkan coba lagi';
    $result = Util::defaultResult($status, $msg);
    return $result;
}

add_action(
    'rest_api_init',
    function () {
        register_rest_route('sedekah', 'index', array('callback' => 'sedekahIndex', 'methods'  => 'POST',));
        register_rest_route('sedekah', 'index', array('callback' => 'sedekahIndex', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'save', array('callback' => 'sedekahSave', 'methods'  => 'POST',));
        register_rest_route('sedekah', 'konfirmasi', array('callback' => 'sedekahKonfirmasi', 'methods'  => 'POST',));
        register_rest_route('sedekah', 'diterima', array('callback' => 'sedekahKonfirmasi', 'methods'  => 'POST',));
        register_rest_route('sedekah', 'mine', array('callback' => 'sedekahMine', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'delete/mine', array('callback' => 'sedekahDeleteMine', 'methods'  => 'POST',));
        register_rest_route('sedekah', 'reset', array('callback' => 'sedekahReset', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'sedekahGetByTanggal', array('callback' => 'sedekahGetByTanggal', 'methods'  => 'POST',));
        register_rest_route('sedekah', 'sedekahGetByTanggal', array('callback' => 'sedekahGetByTanggal', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'summary/tgl/one', array('callback' => 'sedekahAllDaily', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'summary/daily', array('callback' => 'sedekahAllDaily', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'all/1', array('callback' => 'sedekahGetAll1', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'all/2', array('callback' => 'sedekahGetAll2', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'sedekahGetById', array('callback' => 'sedekahGetById', 'methods'  => 'GET',));
        register_rest_route('donatur', 'session', array('callback' => 'donaturSession', 'methods'  => 'GET',));
        register_rest_route('donatur', 'login', array('callback' => 'donaturLogin', 'methods'  => 'POST',));
        register_rest_route('donatur', 'all/1', array('callback' => 'donaturGetAll1', 'methods'  => 'GET',));
        register_rest_route('master', 'paket', array('callback' => 'masterPaket', 'methods'  => 'GET',));
        register_rest_route('master', 'paket/nama', array('callback' => 'masterPaketNama', 'methods'  => 'GET',));
        register_rest_route('sedekah', 'test', array('callback' => 'sedekahTest', 'methods'  => 'GET',));
        register_rest_route('profile', 'one', array('callback' => 'profileOne', 'methods'  => 'GET',));
        register_rest_route('profile', 'edit', array('callback' => 'profileEdit', 'methods'  => 'POST',));
    }
);

