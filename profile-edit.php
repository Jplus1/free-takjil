<?php /* Template Name: Edit Profile */

use radiate\Util;

global $wpdb;

get_header(); 

?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">


        <article id="post-234" class="post-234 page type-page status-publish hentry">
            <header class="entry-header">
                <a id="page-title"></a>
                <h1 class="entry-title">Edit Profile</h1>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                Data Anda
                            </div>
                            <div class="card-body cardDataAnda">
                                <form class="form-edit-profile">
                                    <table class="table">
                                        <tr>
                                            <td>Nama</td>
                                            <td>
                                                <input type="text" name="nama" class="inputNama form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Nama Samaran</td>
                                            <td>
                                                <input type="text"  name="nama_samaran" class="inputNamaSamaran form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Telp</td>
                                            <td>
                                                <input type="text"  name="telp" class="inputTelp form-control"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Alamat</td>
                                            <td>
                                                <textarea  name="alamat" class="inputAlamat form-control"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <input type="submit" class="btn btn-primary" value="Simpan"/>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div><!-- .entry-content -->
           <footer class="entry-meta">
           </footer>
       </article>

   </main><!-- #main -->
</div><!-- #primary -->

<?php 
    $versi_resource = VERSI_RESOURCE; 
    $id = Util::getGetInt("id");
?>
<script>
    var GET_id = <?=json_encode($id)?>;
</script>
<script type="text/javascript" src="/wp-content/themes/radiate/js/profileEdit.js?v=<?=$versi_resource?>"></script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>