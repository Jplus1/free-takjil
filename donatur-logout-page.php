<?php /* Template Name: Donatur Logout */ 

require_once "Util.php";
use radiate\Util;
Util::sessionStart();   

// //simpan session dulu
Util::setSession("donaturId", "");
Util::setSession("donaturNama", "");
Util::setSession("isRelawan", "");
Util::setSession("is_relawan", "");
Util::setSession("donaturNamaSamaran", "");
Util::setSession("donaturTelp", "");
Util::setSession("donaturAlamat", "");

?>
<html>
<head>
    <title>Donatur Logout</title>
</head> 
<body>
    <script>
        localStorage.donaturId= '';
        localStorage.donaturNama= '';
        localStorage.isRelawan= '';
        localStorage.donaturNamaSamaran= '';
        localStorage.donaturTelp= '';
        localStorage.donaturAlamat= '';

        window.location.href = '/sedekah';
    </script>
</body>
</html>



