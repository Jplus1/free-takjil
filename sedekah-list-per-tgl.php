
    <div class="modal fade" id="modal-sedekah-per-tgl" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Daftar Sedekah</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-hover" id="table-detail-per-tgl">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Donatur</th>
                            <th>Jenis Donasi</th>
                            <th class="text-right">Jumlah Porsi</th>
                            <th class="khususRelawan-off">Sudah Konfirmasi</th>
                            <th class="khususRelawan-off">Sudah Diterima</th>
                        </tr>
                    </thead>
                    <tbody class="tbody-list-detail-per-tgl">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <button style="display:none;" type="button" data-tgl='' class="btn btn-secondary btnExportTableToExcel">Export To Excel</button>
            </div>
         </div>
        </div>
    </div>


    <div class="modal fade" id="modal-donasi-all" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">List Donasi</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table-donasi-all">
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
            </div>
         </div>
        </div>
    </div>