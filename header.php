<?php

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package ThemeGrill
 * @subpackage Radiate
 * @since Radiate 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="https://gmpg.org/xfn/11">
<meta property="og:image" content="https://bakubantu.com/wp-content/uploads/2021/02/berbagi-takjil-1-1.jpg">
<link type="image/x-icon" rel="shortcut icon" href="https://bakubantu.com/static/images/favicon.ico"/>
<?php wp_head(); ?>
<script type='text/javascript' src='https://bakubantu.com/static/css/bootstrap.bundle.min.js?ver=5.6.1' id='bootstrap.bundle.min.js-js'></script>
</head>
<body <?php body_class(); ?>>

<?php
if ( function_exists( 'wp_body_open' ) ) {
	wp_body_open();
}
?>



<header id="masthead" class="site-header" role="banner">

    <p class="show-on-mobile-only">
        
    </p>


    <div class="header-wrap clearfix">
        <div class="site-branding">
            <?php if ( is_front_page() || is_home() ) : ?>
                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
            <?php else : ?>
                <h3 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h3>
            <?php endif; ?>
            <?php
            $description = get_bloginfo( 'description', 'display' );
            if ( $description || is_customize_preview() ) : ?>
                <span class="site-description"><?php echo $description; ?></span>
            <?php endif;?>
        </div>

        <?php if ( get_theme_mod( 'radiate_header_search_hide', 0 ) == 0 ) { ?>
            <div class="header-search-icon"></div>
            <?php get_search_form();
        } ?>

        <nav id="site-navigation" class="main-navigation" role="navigation">
            <h4 class="menu-toggle"></h4>

            <?php
            wp_nav_menu(
                array(
                    'theme_location'  => 'primary',
                    'menu_class'      => 'clearfix ',
                    'container_class' => 'menu-primary-container'
                )
            );
            ?>
        </nav><!-- #site-navigation -->
    </div><!-- .inner-wrap header-wrap -->
</header><!-- #masthead -->

<div id="parallax-bg">
	<?php if ( get_header_image() && function_exists( 'the_custom_header_markup' ) && is_front_page() && has_header_video() ) :
		the_custom_header_markup();
	endif; ?>
</div>

<?php
$isRelawan = isset($_SESSION["isRelawan"]) ? $_SESSION["isRelawan"] : "";
if(!$isRelawan) {
    ?>
<style>
    .khususRelawan {
        display: none;
        display: none !important;
    }
</style>
    <?php 
}


require_once "modalBootstrap.html";
require_once "text-on-top.php";
require_once "imgPopup.html";
require_once "sedekah-list-per-tgl.php";
require_once "form-konfirmasi-sedekah.php";
require_once "detail-sedekah.php";
?>

<div id="page" class="hfeed site">


	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'radiate' ); ?></a>

	<?php do_action( 'before' ); ?>

	<div id="content" class="site-content">
		<div class="inner-wrap">
