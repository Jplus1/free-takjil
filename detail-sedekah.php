    <div class="modal fade" id="modalDetailSedekah" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Detail Sedekah</h5>
            <button type="button" class="btn-close btnCloseDetailSedekah" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <table class="table table-hover" >
                    <tbody class="">
                        <tr>
                            <td>Nama Donatur: <span class="font-weight-bold tdNamaDonatur"></span></td>
                            <td>Telp: <span class="font-weight-bold tdTelpDonatur"></span></td>
                        </tr>
                        <tr>
                            <td>Jenis Donasi: <span class="font-weight-bold tdJenisDonasi"></span></td>
                            <td>Jumlah Porsi: <span class="font-weight-bold tdJumlahPorsi"></span></td>
                        </tr>
                        <tr>
                            <td>Dikonfirmasi: <span class="font-weight-bold tdKonfirmasi"></span></td>
                            <td>Diterima: <span class="font-weight-bold tdDiterima"></span></td>
                        </tr>
                    </tbody>
                </table>
                <table class="tableDetail" style="display: none;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Donasi</th>
                            <th class="text-right">Jumlah</th>
                            <th class="text-right">Harga</th>
                            <th class="text-right">SubTotal</th>
                        </tr>
                    </thead>
                    <tbody class="tbodyDetail"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btnCloseDetailSedekah" data-bs-dismiss="modal">Tutup</button>
            </div>
         </div>
        </div>
    </div>