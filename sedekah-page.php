<?php /* Template Name: Sedekah */ ?>

<?php
global $wpdb;

get_header(); 

?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">


        <article id="post-234" class="post-234 page type-page status-publish hentry">
            <header class="entry-header">
                <a id="page-title"></a>
                <h1 class="entry-title">Sedekah</h1>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <div class="row">
                    <div class="col-md-6 col-sm-12 float-right pull-right">
                        <div class="card">
                            <div class="card-header">
                                Data Anda
                            </div>
                            <div class="card-body cardDataAnda">
                                <div class="dataDonaturNotLoggedin ">
                                    <p>
                                        Jika Anda sudah daftar, Anda bisa ketik No HP Anda disini untuk login. 
                                    </p>
                                    <div class="row">
                                        <div class="col-md-8 col-6">
                                            <form class="form-login-donatur" >
                                                <div class="input-group mb-3">
                                                  <input type="text" class="form-control" name="kodeAkses" required="required">
                                                  <div class="input-group-append">
                                                    <input type="submit" class="btn btn-outline-secondary" value="Login"/>
                                                  </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4 col-6">

                                        </div>
                                    </div>
                                </div>
                                <div class="dataDonaturLoggedin infoProfileDonatur" style="display: none;">
                                    
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6 col-sm-12 float-left pull-left" >
                        <div class="card">
                            <div class="card-header">
                                Cara bersedekah:
                            </div>
                            <div class="card-body">
                                1. Klik pada tanggal yang Anda inginkan untuk menyumbang.<br/>
                                2. Isi sedekah Anda.<br/>
                                3. Simpan.<br/>
                                <br/>
                                Keterangan Warna:
                                <br/>
                                <span><i class="fas fa-heart" style='color: red'></i>: jumlah takjil terisi donasi uang</span>
                                <br/>
                                <span><i class="fas fa-heart" style='color: #007bff;'></i>: jumlah takjil terisi donasi makanan</span>
                                <br/>
                                <span><i class="fas fa-heart" style='color: #28a745;'></i>: jumlah takjil tersedia bagi donasi anda</span>

                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row row-semua row-1">
                    <div class="col-12">

                        <table class="table table-hover table-list-sedekah">
                            <thead>
                                <tr>
                                    <th class="kolom-tgl">Hari / Tgl</th>
                                    <th >Target: 700 porsi / Hari</th>
                                </tr>
                            </thead>
                            <tbody class="tbody-list-sedekah">
                           </tbody>
                       </table>
                   </div>
               </div>
                <!-- <div class="row mt-5">
                    <div class="col-md-12 col-sm-12">
                        <div class="card">
                            <div class="card-header">
                                Keterangan Warna
                            </div>
                            <div class="card-body">
                            <span><i class="fas fa-heart" style='color: red'></i>: jumlah takjil terisi donasi uang</span>
                            <br/>
                            <span><i class="fas fa-heart" style='color: #007bff;'></i>: jumlah takjil terisi donasi makanan</span>
                            <br/>
                            <span><i class="fas fa-heart" style='color: #28a745;'></i>: jumlah takjil tersedia bagi donasi anda</span>
                            </div>
                        </div>
                    </div>
                </div> -->
                

           </div><!-- .entry-content -->
           <footer class="entry-meta">
           </footer>
       </article>

   </main><!-- #main -->
</div><!-- #primary -->

<!-- Form Sedekah -->
<form class="form-sedekah">
    <!-- Modal -->

    <div class="modal fade" id="modal-sedekah" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="label-form-sedekah">Form Sedekah</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="inputDonaturId" class="form-control inputDonaturId" id="inputDonaturId">
                <input type="hidden" name="tanggal" class="form-control inputTanggal" id="inputTanggal">
                <table class="table table-hover">
                    <tbody>
                        <tr class='tr-info'>
                            <td colspan="2">
                                <span class="info-sedekah"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="label">Tipe Donasi</span>
                            </td>
                            <td>
                                <label class="btn btn-default">
                                    <input type="radio" name="inputJenisDonasi" class="inputJenisDonasi inputJenisDonasi1" value="1" checked="checked"  required="required"/>
                                    Makanan
                                </label>    
                                <label class="btn btn-default">
                                    <input type="radio" name="inputJenisDonasi" class="inputJenisDonasi inputJenisDonasi2" value="2"  required="required"/>
                                    Uang
                                </label>    
                            </td>
                        </tr>
                        <tr class="trJumlahDonasi" >
                            <td>
                                <span class="label">Jumlah</span>
                            </td>
                            <td>
                                <select name="jumlah" class="selectJumlahPorsiSedekah form-control generateJumlah inputJumlah" ></select>
                            </td>
                        </tr>
                        <tr class="trPaket trPaketMakanan" style="display: none;">
                            <td colspan="2">
                                <p class="text-center bg-light">
                                    <span class="label text-info">Pilih Paket Makanan</span>
                                </p>
                                <div class="listPaket">
                                </div>
                            </td>
                        </tr>
                        <tr class="trPaket trPaketSnack" style="display: none;">
                            <td colspan="2">
                                <p class="text-center bg-light">
                                    <span class="label text-info">Pilih Paket Snack</span>
                                </p>
                                <div class="listPaket">

                                </div>
                            </td>
                        </tr>
                        <tr class="trPaket trPaketTotal" style="display: none;">
                            <td colspan="2">
                                <div class="listPaket">
                                    <div class="row mb-2">
                                        <div class="col-md-7 col-sm-12">
                                            <label class="">Total Jumlah Makanan</label>
                                        </div>
                                        <div class="col-md-5 col-sm-12 text-right">
                                            <input type="hidden" name="inputTotalJumlahMakanan" class="inputTotalJumlahMakanan" value=""/>
                                            <span class="totalJumlahMakanan">0</span>
                                        </div>
                                    </div>
                                    <div class="row mb-2">
                                        <div class="col-md-7 col-sm-12">
                                            <label class="">Total Nilai Rupiah</label>
                                        </div>
                                        <div class="col-md-5 col-sm-12 text-right">
                                            <input type="hidden" name="inputTotalJumlahUang" class="inputTotalJumlahUang" value=""/>
                                            <span class="totalJumlahUang">0</span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>Data Anda</h2>
                            </td>
                        </tr>
                        <tr class='tr-donatur tr-donatur-nama'>
                            <td>
                                <span class="label">Nama Anda</span>                                
                            </td>
                            <td>
                                <input type="text" name="nama" class="form-control inputNama" maxlength="50"  required="required"/>
                                <br/>
                                <label class="label">
                                    <input type="checkbox" value="1" class="inputGunakanNamaSamaran" /> Gunakan Nama Samaran
                                </label>
                            </td>
                        </tr>
                        <tr class='tr-donatur tr-donatur-nama-samaran' style="display:none;">
                            <td>
                                <span class="label">Nama Samaran</span>
                            </td>
                            <td>
                                <input type="text" name="namaSamaran" class="form-control inputNamaSamaran" maxlength="100" value="Hamba Allah"/>
                            </td>
                        </tr>
                        <tr class='tr-donatur tr-donatur-telp'>
                            <td>
                                <span class="label">No HP / WA</span>                                
                            </td>
                            <td>
                                <input type="text" name="telp" class="form-control inputTelp" maxlength="50"  required="required"/>
                            </td>
                        </tr>
                        <tr class='tr-donatur tr-donatur-alamat'>
                            <td>
                                <span class="label">Alamat</span>                                
                            </td>
                            <td>
                                <input type="text" name="alamat" class="form-control inputAlamat" maxlength="50"  required="required"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                <input type="submit" class="btn btn-primary" value="Simpan"/>
            </div>
         </div>
        </div>
    </div>
</form>
<!-- Form Sedekah -->   

<?php 
    $versi_resource = VERSI_RESOURCE; 
?>
<?php get_sidebar(); ?>








<!-- <link  rel='stylesheet' type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" /> -->
<link  rel='stylesheet' type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap5.min.css" />

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/radiate/js/sedekah.js?v=<?=$versi_resource?>"></script>
<?php get_footer(); ?>