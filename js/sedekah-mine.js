let urlSedekahMine = '/sedekah-mine';
let urlSedekahIndex = '/wp-json/sedekah/index';
let urlSedekahMineApi = '/wp-json/sedekah/mine';
let urlSedekahSave = '/wp-json/sedekah/save';
let urlSedekahDeleteMine = '/wp-json/sedekah/delete/mine';
let urlSedekahGetByTanggal = '/wp-json/sedekah/sedekahGetByTanggal?tgl={tgl}';
let urlDonaturSession = '/wp-json/donatur/session';
let urlDonaturLogin = '/wp-json/donatur/login';
let urlDonaturLogout = '/donatur-logout';
let urlMasterPaket = '/wp-json/master/paket';

let MASTER_PAKET = [];

let getSessionDonatur = function() {
	return $.get(urlDonaturSession);
}

let generateJumlah = function (elementClass) {
	let theClass;
	if(elementClass) {
		theClass = elementClass;
	} else {
		theClass = 'generateJumlah';
	}

	inputJumlah = $(document).find(`.${theClass}`);
	inputJumlah.empty();
	
	let inputJumlahAllowEmpty = $(document).find(`.${theClass}[data-allowEmpty='1']`);
	let option = `<option value=""></option>`;
	inputJumlahAllowEmpty.append(option);
	for(i=10; i<=700; i=i+10) {
		let option = `<option value="${i}">${i}</option>`;
		inputJumlah.append(option);
	}
}

let runScript2 = function () {


        function setLoggedInDonatur(donatur) {
            let logos = `Selamat Datang ${donatur.nama} <br/>`;

            if(donatur.namaSamaran && donatur.nama != donatur.namaSamaran) {
                logos += `Nama Samaran: ${donatur.namaSamaran} <br/>`;
            }
            logos += `Telp: ${donatur.telp} <br/>`;
            logos += `Alamat: ${donatur.alamat} <br/>`;
            logos += `<br/>`;
            //logos += `<a href="${urlSedekahMine}" class="btn btn-primary btn-lg mr-2"><i class="fas fa-praying-hands"></i>Sedekah Anda</a>`;
            logos += `<a href="${urlDonaturLogout}" class="btn btn-primary btn-lg btn-logout-donatur"><i class="fas fa-sign-out-alt"></i> Log Out</a>`;

            let infoProfileDonatur = $(".infoProfileDonatur");
            infoProfileDonatur.html(logos);

            $(".dataDonaturLoggedin").show();
            $(".dataDonaturNotLoggedin").hide();
        }

    function initDataProfile() {
        let cardDataAnda = $(".cardDataAnda");
        if(localStorage.donaturId) {
            let donatur = {};
            donatur.nama = localStorage.donaturNama;
            donatur.namaSamaran = localStorage.donaturNamaSamaran;
            donatur.telp = localStorage.donaturTelp;
            donatur.alamat = localStorage.donaturAlamat;

            setLoggedInDonatur(donatur);
        }
    }
    initDataProfile();


    
    function initTableSedekahMine() {
        $.get(urlSedekahMineApi).done(function function_name(result) {
            let tbody = $(".tbody-list-sedekah");
            tbody.empty();
            let isMine = tbody.data('mine');



            let templateKosong = ''
            + '<tr>'
            + '        <td colspan="5">'
            + '            <h3 class="mt-5 mb-5 text-danger">Anda belum menyumbangkan sedekah Anda</h3>'
            + '            <br/>Silahkan <a href="/sedekah">klik disini</a> untuk bersedekah.'
            + '        </td>'
            + '    </tr>'
            + '';
            if(result.data.length === 0) {
                tbody.append(templateKosong);
                return true;
            }


            let templateOne = ''
            + '<tr>'
            + '        <td>'
            + '            {tgl_show}'
            + '        </td>'
            + '        <td>'
            + '            {formatted_jumlah_porsi_sedekah_makanan}'
            + '        </td>'
            + '        <td>'
            + '                     {formatted_jumlah_porsi_sedekah_uang}'
            + '        </td>'
            + '        <td>'
            + '               {formatted_jumlah_porsi_total_sedekah}'
            + '        </td>'
            + '        <td>'
            + '               <span class="btn btn-danger btn-hapus-sedekah" data-id="{id}" data-tgl="{tgl}" >Hapus</span>'
            + '        </td>'
            + '    </tr>'
            + '';

            let jumlah_target_takjil_harian = parseInt(result.jumlah_target_takjil_harian);
            if(jumlah_target_takjil_harian <= 0) {
                jumlah_target_takjil_harian = 700;
            }
            let listData = result.data;

            for(x=0; x < result.data.length; x++) {
            // $.each(listData, function function_name(index, item) {
                let item = result.data[x];
                // x++;
                let jumlah_porsi_total_target = jumlah_target_takjil_harian;
                // console.log('jumlah_porsi_total_target: ', jumlah_porsi_total_target);


                let jumlah_porsi_sedekah_makanan = parseInt(item.jumlah_porsi_sedekah_makanan);

                // console.log('jumlah_porsi_sedekah_makanan: ', jumlah_porsi_sedekah_makanan);
                let persentase_jumlah_porsi_sedekah_makanan = (jumlah_porsi_sedekah_makanan / jumlah_porsi_total_target * 100).toFixed(0);
                // console.log('persentase_jumlah_porsi_sedekah_makanan: ', persentase_jumlah_porsi_sedekah_makanan);
                let formatted_jumlah_porsi_sedekah_makanan = JQCustom.numberFormatIndonesia(jumlah_porsi_sedekah_makanan);
                // console.log('formatted_jumlah_porsi_sedekah_makanan: ', formatted_jumlah_porsi_sedekah_makanan);

                let jumlah_porsi_sedekah_uang = parseInt(item.jumlah_porsi_sedekah_uang);
                // console.log('jumlah_porsi_sedekah_uang: ', jumlah_porsi_sedekah_uang);
                let persentase_jumlah_porsi_sedekah_uang = (jumlah_porsi_sedekah_uang / jumlah_porsi_total_target * 100).toFixed(0);
                // console.log('persentase_jumlah_porsi_sedekah_uang: ', persentase_jumlah_porsi_sedekah_uang);
                let formatted_jumlah_porsi_sedekah_uang = JQCustom.numberFormatIndonesia(jumlah_porsi_sedekah_uang);
                // console.log('formatted_jumlah_porsi_sedekah_uang: ', formatted_jumlah_porsi_sedekah_uang);

                let jumlah_porsi_total_sedekah = jumlah_porsi_sedekah_makanan + jumlah_porsi_sedekah_uang;

                // console.log('jumlah_porsi_total_sedekah: ', jumlah_porsi_total_sedekah);
                let persentase_jumlah_porsi_total_sedekah = (jumlah_porsi_total_sedekah / jumlah_porsi_total_target * 100).toFixed(0);
                // console.log('persentase_jumlah_porsi_total_sedekah: ', persentase_jumlah_porsi_total_sedekah);
                let formatted_jumlah_porsi_total_sedekah = JQCustom.numberFormatIndonesia(jumlah_porsi_total_sedekah);
                // console.log('formatted_jumlah_porsi_total_sedekah: ', formatted_jumlah_porsi_total_sedekah);

                let jumlah_porsi_total_kosong = jumlah_porsi_total_target - jumlah_porsi_total_sedekah;
                // console.log('jumlah_porsi_total_kosong: ', jumlah_porsi_total_kosong);
                let persentase_jumlah_porsi_total_kosong = (jumlah_porsi_total_kosong / jumlah_porsi_total_target * 100).toFixed(0);
                // console.log('persentase_jumlah_porsi_total_kosong: ', persentase_jumlah_porsi_total_kosong);
                let formatted_jumlah_porsi_total_kosong = JQCustom.numberFormatIndonesia(jumlah_porsi_total_kosong);
                // console.log('formatted_jumlah_porsi_total_kosong: ', formatted_jumlah_porsi_total_kosong);

                let bg_kosong = "bg-danger";

                let tgl = item.tgl;

                let listTanggal = tgl.split('-');
                let tgl_show = listTanggal[2] + '-' + listTanggal[1] + '-' + listTanggal[0];


                let one = templateOne + '';
                one = JQCustom.replaceAll('{id}', item.id, one);
                one = JQCustom.replaceAll('{tgl}', tgl, one);
                one = JQCustom.replaceAll('{tgl_show}', tgl_show, one);
                // one = one.replaceAll('{tgl}', tgl_show);

                one = JQCustom.replaceAll('{jumlah_porsi_sedekah_makanan}', jumlah_porsi_sedekah_makanan, one);
                one = JQCustom.replaceAll('{persentase_jumlah_porsi_sedekah_makanan}', persentase_jumlah_porsi_sedekah_makanan, one);
                one = JQCustom.replaceAll('{formatted_jumlah_porsi_sedekah_makanan}', formatted_jumlah_porsi_sedekah_makanan, one);
                one = JQCustom.replaceAll('{jumlah_porsi_sedekah_uang}', jumlah_porsi_sedekah_uang, one);
                                one = JQCustom.replaceAll('{persentase_jumlah_porsi_sedekah_uang}', persentase_jumlah_porsi_sedekah_uang, one);
                                one = JQCustom.replaceAll('{formatted_jumlah_porsi_sedekah_uang}', formatted_jumlah_porsi_sedekah_uang, one);
                one = JQCustom.replaceAll('{jumlah_porsi_total_kosong}', jumlah_porsi_total_kosong, one);
                one = JQCustom.replaceAll('{bg_kosong}', bg_kosong, one);
                one = JQCustom.replaceAll('{persentase_jumlah_porsi_total_kosong}', persentase_jumlah_porsi_total_kosong, one);
                one = JQCustom.replaceAll('{formatted_jumlah_porsi_total_kosong}', formatted_jumlah_porsi_total_kosong, one);
                one = JQCustom.replaceAll('{jumlah_porsi_total_sedekah}', formatted_jumlah_porsi_total_sedekah, one);
                one = JQCustom.replaceAll('{formatted_jumlah_porsi_total_sedekah}', formatted_jumlah_porsi_total_sedekah, one);
                                tbody.append(one);
            // }); //end foreach
                    }; //end foreach
        }); //end ajax
    };//end function

    
    initTableSedekahMine();



        $(document).on("click", ".btn-hapus-sedekah", function (e) {
            let me = $(this),
            tgl = me.data('tgl');
                let listTanggal = tgl.split('-');
                let tgl_show = listTanggal[2] + '-' + listTanggal[1] + '-' + listTanggal[0];


            if(!confirm(`Apakah Anda yakin ingin menghapus data Anda pada tanggal: ${tgl_show}?`)) return false;

            let data = {
                tgl: tgl,
            };
            $.post(urlSedekahDeleteMine, data)
            .done(function function_name(result) {
                if(result.status) {
                    $(".tr-is-mine").remove();
                    // window.location.reload();
                    initTableSedekahMine();
                } else {
                    modalShow(result.message || 'Maaf terjadi kesalahan, silahkan coba lagi');
                }
            })
        });
    };

    function runScript() {
        // Script that does something and depends on jQuery being there.
        // if( window.$ ) {
	if( typeof jQuery !== 'undefined' ) {
        	window.$ = jQuery;
        	runScript2();
	} else {
            window.setTimeout( runScript, 100 );
        }
    }

    runScript();