let urlProfile = '/profile';
let urlProfileOneApi = '/wp-json/profile/one';
let urlProfileEditApi = '/wp-json/profile/edit';

function readyEditProfile() {
	function prepareOldData() {
		$.get(urlProfileOneApi)
		.done(function(result) {
			if(result.status) {
				let one = result.data;

				let me = $(this),
				inputNama = $(".inputNama"),
				inputNamaSamaran = $(".inputNamaSamaran"),
				inputTelp = $(".inputTelp"),
				inputAlamat = $(".inputAlamat");

				inputNama.val(one.nama)
				inputNamaSamaran.val(one.nama_samaran)
				inputTelp.val(one.telp)
				inputAlamat.val(one.alamat)


			} else {
				modalShow(result.message || 'Maaf, data Anda tidak ditemukan.');
			}
		})
	}

	prepareOldData();

	$(document).on("submit", ".form-edit-profile", function name(e) {
		e.preventDefault();

		let me = $(this),
		param = me.serializeArray(),
		inputNama = $(".inputNama"),
		inputNamaSamaran = $(".inputNamaSamaran"),
		inputTelp = $(".inputTelp"),
		inputAlamat = $(".inputAlamat");

		let nama = inputNama.val();
		let nama_samaran = inputNamaSamaran.val();
		let telp = inputTelp.val();
		let alamat = inputAlamat.val();


		$.post(urlProfileEditApi, param)
		.done(function name(result) {
			if(result.status) {
				localStorage.donaturNama = nama;
				localStorage.donaturNamaSamaran = nama_samaran;
				localStorage.donaturTelp = telp;
				localStorage.donaturAlamat = alamat;

				alert('Data berhasil disimpan');

				window.location.href = '/sedekah';
			} else {
				modalShow(result.message || 'Data gagal tersimpan. Mohon coba lagi');
			}
		})
	})
}


function prepareEditProfile() {
	if( typeof jQuery !== 'undefined' ) {
		window.$ = jQuery;
		readyEditProfile();
	} else {
		window.setTimeout(prepareEditProfile, 100 );
	}
}
prepareEditProfile();