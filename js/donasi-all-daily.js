let urlSedekahMine = '/sedekah-mine';
let urlProfileEdit = '/profile-edit';

let urlSedekahIndex = '/wp-json/sedekah/index';
let urlSedekahMineApi = '/wp-json/sedekah/mine';
let urlSedekahSave = '/wp-json/sedekah/save';
let urlSedekahDeleteMine = '/wp-json/sedekah/delete/mine';
let urlSedekahGetByTanggal = '/wp-json/sedekah/sedekahGetByTanggal?tgl={tgl}';
let urlSedekahGetById = '/wp-json/sedekah/sedekahGetById?id={id}';
let urlSedekahKonfirmasi = '/wp-json/sedekah/konfirmasi';
let urlSedekahDiterima = '/wp-json/sedekah/diterima';

let urlDonaturSession = '/wp-json/donatur/session';
let urlDonaturLogin = '/wp-json/donatur/login';
let urlDonaturLogout = '/donatur-logout';
let urlMasterPaket = '/wp-json/master/paket';

let MASTER_PAKET = [];

let getSessionDonatur = function() {
	return $.get(urlDonaturSession);
}

function doTableIndex() {
	let tableId = '#table-index';
	if($.fn.dataTable.isDataTable(tableId)) {
		$(tableId).DataTable().clear().destroy();
		$(tableId).empty();
	}
	$(tableId).hide();

	let urlPaket = '/wp-json/master/paket/nama';
	$.get(urlPaket).done(function name(result) {
		if(!result.status) {
			modalShow(result.message || 'Data gagal diload, silahkan coba lagi');
			return;
		}

		let url = '/wp-json/sedekah/summary/daily';
		let title = 'List Donasi - Daily';
					
		let columns = [];
		columns.push({'title': 'Tgl Y-m-d', 'data': 'tgl', visible: false});
		columns.push({'title': 'Tgl', 'data': 'tgl_dmy'});
		columns.push({'title': 'Jumlah Donasi Uang', 'data': 'jumlah_rupiah'});
		columns.push({'title': 'Jumlah Donasi Makanan', 'data': 'jumlah_non_paket'});
		
		//Tgl | Jumlah Porsi | Paket A, PAKET B, PAKET C 
		let columns_tidak_dihitung = [];
		$.each(result.data, function name(index, item) {
			if(item.is_dihitung != "1") {
				columns_tidak_dihitung.push(columns.length);
			}
			columns.push({title: item.nama, data: 'paket_' + item.id});
		})
		columns.push({'title': 'Jumlah Porsi', 'data': 'jumlah_porsi'});

		let param = {};

		let dataIndex = [];
		$.get(url, param) 
		.done(function name(result) {
			if(!result.status) {
				modalShow(result.message || 'Data load gagal, silahkan coba lagi.');
				return false;
			}

			let exportOptions = [];
			for(i=0; i<columns.length; i++) {
				if(i > 0) {
					exportOptions.push(i);
				}
			}
			console.log('exportOptions: ', exportOptions);

			$(tableId).DataTable( {
				"paging":   false,
				"bInfo" : false,
				data: result.data,
				dom: 'frtipB',
				// bAutoWidth: false,
				buttons: [
					{
						extend: 'excelHtml5',
						title: title,
						exportOptions: {
							columns: exportOptions
						}
					},
					{
						extend: 'pdfHtml5',
						title: title,
						exportOptions: {
							columns: exportOptions
						}
					}
				],
				columns: columns,
				"columnDefs": [ {
					"targets": columns_tidak_dihitung,
					"createdCell": function (td, cellData, rowData, row, col) {
						// $(td).css('color', 'red');
						$(td).css('background-color', '#f5d4c4');
					}
				  } ]
			} );

			$(tableId).show();

		})

	});

}

function runScript2(params) {
	doTableIndex();


	$(document).on("click", ".btn-go", function name(e) {
		e.preventDefault();

		doTableIndex();
	})
}

function runScript() {
	// Script that does something and depends on jQuery being there.
	// if( window.$ ) {
	if( typeof jQuery !== 'undefined' ) {
			window.$ = jQuery;
			runScript2();
	} else {
		window.setTimeout( runScript, 100 );
	}
}

runScript();