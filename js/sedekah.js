let urlSedekahMine = '/sedekah-mine';
let urlProfileEdit = '/profile-edit';


let urlSedekahIndex = '/wp-json/sedekah/index';
let urlSedekahMineApi = '/wp-json/sedekah/mine';
let urlSedekahSave = '/wp-json/sedekah/save';
let urlSedekahDeleteMine = '/wp-json/sedekah/delete/mine';
let urlSedekahGetByTanggal = '/wp-json/sedekah/sedekahGetByTanggal?tgl={tgl}';
let urlSedekahGetById = '/wp-json/sedekah/sedekahGetById?id={id}';
let urlSedekahKonfirmasi = '/wp-json/sedekah/konfirmasi';
let urlSedekahDiterima = '/wp-json/sedekah/diterima';

let urlDonaturSession = '/wp-json/donatur/session';
let urlDonaturLogin = '/wp-json/donatur/login';
let urlDonaturLogout = '/donatur-logout';
let urlMasterPaket = '/wp-json/master/paket';

let MASTER_PAKET = [];

let getSessionDonatur = function() {
	return $.get(urlDonaturSession);
}

function showHideKhususRelawan() {
	if(localStorage.isRelawan) {
		$(".khususRelawan").show();
	} else {
		$(".khususRelawan").hide();
	}
}


let generateJumlah = function (elementClass, limit) {
	if(typeof(limit) === 'undefined') {
		limit = 700;
	}
	limit = parseInt(limit);

	if(localStorage.isRelawan) {
		limit = 700; //khusus buat admin, biar aja tetep 700
	}

	let theClass;
	if(elementClass) {
		theClass = elementClass;
	} else {
		theClass = 'selectJumlahPorsiSedekah';
	}

	inputJumlah = $(document).find(`.${theClass}`);
	inputJumlah.empty();

	if(limit <= 0) return;
	
	let inputJumlahAllowEmpty = $(document).find(`.${theClass}[data-allowEmpty='1']`);
	let option = `<option value=""></option>`;
	inputJumlahAllowEmpty.append(option);
	let limitSudahMasuk = false;

	for(i=10; i<=limit; i=i+10) {
		let option = `<option value="${i}">${i}</option>`;
		inputJumlah.append(option);

		if(i === limit) {
			limitSudahMasuk = true;
		}
	}
	if(!limitSudahMasuk) {
		i = limit;
		let option = `<option value="${i}">${i}</option>`;
		inputJumlah.append(option);
	}
}

let runScript2 = function () {
	$(".form-sedekah").detach().appendTo($("body"));
	$("#modal-sedekah-per-tgl").detach().appendTo($("body"));

	var modalFormSedekah = new bootstrap.Modal(document.getElementById('modal-sedekah'), {
		keyboard: false
	});

	var modalDonasiAll = new bootstrap.Modal(document.getElementById('modal-donasi-all'), {
		keyboard: false
	});

	var modalFormKonfirmasiSedekah = new bootstrap.Modal(document.getElementById('modalKonfirmasiSedekah'), {
		keyboard: true
	});

	let modalSedekahPerTgl = new bootstrap.Modal(document.getElementById('modal-sedekah-per-tgl'), {
		keyboard: true
	});

	let modalDetailSedekah = new bootstrap.Modal(document.getElementById('modalDetailSedekah'), {
		keyboard: true
	});

	function initDataProfile() {
		
		localStorage.donaturId = '';
		localStorage.isRelawan = '';
		localStorage.donaturNama = '';
		localStorage.donaturNamaSamaran = '';
		localStorage.donaturTelp = '';
		localStorage.donaturAlamat = '';
		localStorage.donaturKodeAkses = '';


		$(".khususRelawan").hide();

		$.get(urlDonaturSession)
		.done(function name(result) {
			if(result.status) {
				let data = result.data;

				let isRelawan = parseInt(data.isRelawan) === 1;
				if(isRelawan) {
					$(".khususRelawan").show();
				}
				localStorage.donaturId = data.donaturId;
				localStorage.isRelawan = isRelawan ? 1 : '';
				localStorage.donaturNama = data.donaturNama;
				localStorage.donaturNamaSamaran = data.donaturNamaSamaran;
				localStorage.donaturTelp = data.donaturTelp;
				localStorage.donaturAlamat = data.donaturAlamat;
				localStorage.donaturKodeAkses = data.donaturKodeAkses;


				if(localStorage.donaturId) {
					let donatur = {};
					donatur.nama = localStorage.donaturNama;
					donatur.namaSamaran = localStorage.donaturNamaSamaran;
					donatur.telp = localStorage.donaturTelp;
					donatur.alamat = localStorage.donaturAlamat;

					setLoggedInDonatur(donatur);
				}
			} else {
				modalShow(result.message || 'Load profile gagal, silahkan coba login kembali');
			}
		})
	}
	initDataProfile();
	function initTableSedekah() {
		$.get(urlSedekahIndex).done(function function_name(result) {
			let templateOne = ''
			+ '<tr>'
			+ '        <td class="{td-tgl-class}">'
			+ '            <button class="link-tgl btn btn-link text-lg btn-lg {btnDisabled}" data-spesial="{tgl_is_special}"  data-tgl="{tgl}" data-jumlah="{jumlah_porsi_total_kosong}" >{tgl_show}</button>{sudahFull}'
			+ '        </td>'
			+ '        <td>'
			+ '        	<div  data-tgl="{tgl}" data-tgl_show="{tgl_show}" class="progress progress-parent" style="height: 30px;">'
			+ '            	<div data-tgl="{tgl}" data-jumlah="{jumlah_porsi_sedekah_makanan}" class="progress-clickable progress-isi progress-isi-makanan progress-bar bg-sedekah-makanan" role="progressbar"'
			+ '               		style="width: {persentase_jumlah_porsi_sedekah_makanan}%">'
			+ '                    {formatted_jumlah_porsi_sedekah_makanan}'
			+ '               </div>'
			+ '            <div data-tgl="{tgl}" data-jumlah="{jumlah_porsi_sedekah_uang}" class="progress-clickable progress-isi progress-isi-uang progress-bar  bg-sedekah-uang" role="progressbar"'
			+ '               style="width: {persentase_jumlah_porsi_sedekah_uang}%">'
			+ '               		{formatted_jumlah_porsi_sedekah_uang}'
			+ '            </div>'
			+ '            <div data-tgl="{tgl}" data-jumlah="{jumlah_porsi_total_kosong}" class="progress-clickable progress-kosong progress-bar {bg_kosong}" role="progressbar"'
			+ '               style="width: {persentase_jumlah_porsi_total_kosong}%">'
			+ '               {formatted_jumlah_porsi_total_kosong}'
			+ '            </div>'
			+ '        </div>'
			+ '      </td>'
			+ '    </tr>'
			+ '';

			let jumlah_target_takjil_harian = parseInt(result.jumlah_target_takjil_harian);
			if(jumlah_target_takjil_harian <= 0) {
				jumlah_target_takjil_harian = 700;
			}
			let listData = result.data;

			let tbody = $(".tbody-list-sedekah");
			tbody.empty();

            for(x=0; x < result.data.length; x++) {
			// $.each(listData, function function_name(index, item) {
                let item = result.data[x];
				// x++;

				let totalJumlahPorsi = parseInt(item.jumlah_porsi_sedekah_makanan) + parseInt(item.jumlah_porsi_sedekah_uang);

				let jumlah_porsi_total_target = totalJumlahPorsi > jumlah_target_takjil_harian ? totalJumlahPorsi : jumlah_target_takjil_harian;
				// console.log('jumlah_porsi_total_target: ', jumlah_porsi_total_target);

				let jumlah_porsi_sedekah_makanan = parseInt(item.jumlah_porsi_sedekah_makanan);
				// console.log('jumlah_porsi_sedekah_makanan: ', jumlah_porsi_sedekah_makanan);
				let persentase_jumlah_porsi_sedekah_makanan = (jumlah_porsi_sedekah_makanan / jumlah_porsi_total_target * 100).toFixed(0);
				// console.log('persentase_jumlah_porsi_sedekah_makanan: ', persentase_jumlah_porsi_sedekah_makanan);
				let formatted_jumlah_porsi_sedekah_makanan = JQCustom.numberFormatIndonesia(jumlah_porsi_sedekah_makanan);
				// console.log('formatted_jumlah_porsi_sedekah_makanan: ', formatted_jumlah_porsi_sedekah_makanan);

				let jumlah_porsi_sedekah_uang = parseInt(item.jumlah_porsi_sedekah_uang);
				// console.log('jumlah_porsi_sedekah_uang: ', jumlah_porsi_sedekah_uang);
				let persentase_jumlah_porsi_sedekah_uang = (jumlah_porsi_sedekah_uang / jumlah_porsi_total_target * 100).toFixed(0);
				// console.log('persentase_jumlah_porsi_sedekah_uang: ', persentase_jumlah_porsi_sedekah_uang);
				let formatted_jumlah_porsi_sedekah_uang = JQCustom.numberFormatIndonesia(jumlah_porsi_sedekah_uang);
				// console.log('formatted_jumlah_porsi_sedekah_uang: ', formatted_jumlah_porsi_sedekah_uang);

				let jumlah_porsi_total_sedekah = jumlah_porsi_sedekah_makanan + jumlah_porsi_sedekah_uang;
				// console.log('jumlah_porsi_total_sedekah: ', jumlah_porsi_total_sedekah);
				let persentase_jumlah_porsi_total_sedekah = (jumlah_porsi_total_sedekah / jumlah_porsi_total_target * 100).toFixed(0);
				// console.log('persentase_jumlah_porsi_total_sedekah: ', persentase_jumlah_porsi_total_sedekah);
				let formatted_jumlah_porsi_total_sedekah = JQCustom.numberFormatIndonesia(jumlah_porsi_total_sedekah);
				// console.log('formatted_jumlah_porsi_total_sedekah: ', formatted_jumlah_porsi_total_sedekah);

				let jumlah_porsi_total_kosong = jumlah_porsi_total_target - jumlah_porsi_total_sedekah;
				// console.log('jumlah_porsi_total_kosong: ', jumlah_porsi_total_kosong);
				let persentase_jumlah_porsi_total_kosong = (jumlah_porsi_total_kosong / jumlah_porsi_total_target * 100).toFixed(0);
				// console.log('persentase_jumlah_porsi_total_kosong: ', persentase_jumlah_porsi_total_kosong);
				let formatted_jumlah_porsi_total_kosong = JQCustom.numberFormatIndonesia(jumlah_porsi_total_kosong);
				// console.log('formatted_jumlah_porsi_total_kosong: ', formatted_jumlah_porsi_total_kosong);

				persentase_jumlah_porsi_sedekah_makanan = parseInt( persentase_jumlah_porsi_sedekah_makanan);
				persentase_jumlah_porsi_sedekah_uang = parseInt( persentase_jumlah_porsi_sedekah_uang );
				persentase_jumlah_porsi_total_kosong = parseInt( persentase_jumlah_porsi_total_kosong);


				let minimumPersen = 25;
				let nilaiTambah = 0;
				if(persentase_jumlah_porsi_sedekah_makanan > 0 && persentase_jumlah_porsi_sedekah_makanan <= minimumPersen) {
					nilaiTambah = minimumPersen - persentase_jumlah_porsi_sedekah_makanan;
					persentase_jumlah_porsi_sedekah_makanan += nilaiTambah;	
					if(persentase_jumlah_porsi_sedekah_uang > persentase_jumlah_porsi_total_kosong) {
						persentase_jumlah_porsi_sedekah_uang -= nilaiTambah;	
					} else {
						persentase_jumlah_porsi_total_kosong -= nilaiTambah;	
					}
				}
				if(persentase_jumlah_porsi_sedekah_uang > 0 && persentase_jumlah_porsi_sedekah_uang <= minimumPersen) {
					nilaiTambah = minimumPersen - persentase_jumlah_porsi_sedekah_uang;
					persentase_jumlah_porsi_sedekah_uang += nilaiTambah;	
					if(persentase_jumlah_porsi_sedekah_makanan > persentase_jumlah_porsi_total_kosong) {
						persentase_jumlah_porsi_sedekah_makanan -= nilaiTambah;	
					} else {
						persentase_jumlah_porsi_total_kosong -= nilaiTambah;	
					}
				}
				if(persentase_jumlah_porsi_total_kosong > 0 && persentase_jumlah_porsi_total_kosong <= minimumPersen) {
					nilaiTambah = minimumPersen - persentase_jumlah_porsi_total_kosong;
					persentase_jumlah_porsi_total_kosong += nilaiTambah;	
					if(persentase_jumlah_porsi_sedekah_makanan > persentase_jumlah_porsi_sedekah_uang) {
						persentase_jumlah_porsi_sedekah_makanan -= nilaiTambah;	
					} else {
						persentase_jumlah_porsi_sedekah_uang -= nilaiTambah;	
					}
				}


				// let bg_kosong = "bg-danger";
                let bg_kosong = "bg-sedekah-kosong";

				let sudahFull = ` (${JQCustom.numberFormatIndonesia(totalJumlahPorsi)})`;
				let btnDisabled = '';

				if(localStorage.isRelawan) {
					//do nothing
				} else {
					if(jumlah_porsi_total_kosong <= 0) {
						// sudahFull = '';
						btnDisabled = ' disabled ';
					}
				}

				let tgl = item.tgl;
				let tgl_is_special = item.is_spesial;

				


                let listTanggal = tgl.split('-');
                let tgl_show = listTanggal[2] + '-' + listTanggal[1] + '-' + listTanggal[0];

				let td_tgl_class = '';
				let is_tgl_tutup = parseInt(item.is_tgl_tutup) > 0;
				if(is_tgl_tutup) {
					td_tgl_class = 'bg-mute';
					btnDisabled = ' disabled ';
				}
				let is_hari_ini = parseInt(item.is_hari_ini) > 0;
				if(is_hari_ini) {
					td_tgl_class = 'bg-orange';
					btnDisabled = ' disabled ';
				}

				if(localStorage.isRelawan) {
					btnDisabled = ' ';
				}

				// console.log('tgl: ' + tgl_show + ' | btnDisabled: ', btnDisabled);


				let one = templateOne + '';
				one = JQCustom.replaceAll('{tgl}', tgl, one);
				one = JQCustom.replaceAll('{tgl_is_special}', tgl_is_special, one);
				one = JQCustom.replaceAll('{td-tgl-class}', td_tgl_class, one);
                one = JQCustom.replaceAll('{tgl_show}', tgl_show, one);
                // one = one.replaceAll('{tgl}', tgl_show);

				one = JQCustom.replaceAll('{jumlah_porsi_sedekah_makanan}', jumlah_porsi_sedekah_makanan, one);
				one = JQCustom.replaceAll('{persentase_jumlah_porsi_sedekah_makanan}', persentase_jumlah_porsi_sedekah_makanan, one);
				one = JQCustom.replaceAll('{formatted_jumlah_porsi_sedekah_makanan}', formatted_jumlah_porsi_sedekah_makanan, one);
				one = JQCustom.replaceAll('{jumlah_porsi_sedekah_uang}', jumlah_porsi_sedekah_uang, one);
				one = JQCustom.replaceAll('{persentase_jumlah_porsi_sedekah_uang}', persentase_jumlah_porsi_sedekah_uang, one);
				one = JQCustom.replaceAll('{formatted_jumlah_porsi_sedekah_uang}', formatted_jumlah_porsi_sedekah_uang, one);
				one = JQCustom.replaceAll('{jumlah_porsi_total_kosong}', jumlah_porsi_total_kosong, one);
				one = JQCustom.replaceAll('{bg_kosong}', bg_kosong, one);
				one = JQCustom.replaceAll('{persentase_jumlah_porsi_total_kosong}', persentase_jumlah_porsi_total_kosong, one);
				one = JQCustom.replaceAll('{formatted_jumlah_porsi_total_kosong}', formatted_jumlah_porsi_total_kosong, one);
				one = JQCustom.replaceAll('{sudahFull}', sudahFull, one);
				one = JQCustom.replaceAll('{btnDisabled}', btnDisabled, one);
				tbody.append(one);
			// }); //end foreach
                    }; //end foreach
		}); //end ajax
	};//end function
	initTableSedekah();



        //pindahin form ke dalam <body>
        function initFormSedekah(limitJumlahPaket, tgl_is_spesial) {
			$(".inputNama").val(localStorage.donaturNama);
			$(".inputNamaSamaran").val(localStorage.donaturNamaSamaran);
			$(".inputGunakanNamaSamaran").prop('checked', localStorage.donaturNama != localStorage.donaturNamaSamaran);
			$(".inputTelp").val(localStorage.donaturTelp);
			$(".inputAlamat").val(localStorage.donaturAlamat);


			if(typeof(limitJumlahPaket) === 'undefined') {
				limitJumlahPaket = 700;
			}

			var param = {}
			if(tgl_is_spesial) {
				param.tgl_is_spesial = tgl_is_spesial;
			}

        	//ambil MASTER_PAKET;
        	$.get(urlMasterPaket, param).done(function (result) {
        		if(result.status) {
        			MASTER_PAKET = result.data;


        			let template = ''
        			+'	<div class="row mb-2">'
        			+'        <div class="col-md-7 col-sm-12">'
        			+'            <label class="">{namaPaket}</label>'
        			+'        </div>'
        			+'        <div class="col-md-5 col-sm-12">'
        			+'            <input type="hidden" name="idPaket[]" value="{idPaket}"/>'
        			+'            <input type="hidden" name="hargaPaket[]" value="{hargaPaket}"/>'
        			+'            <select name="jumlahPaket[]" data-harga="{hargaPaket}" data-allowEmpty="1" data-is_dihitung="{is_dihitung}" class="form-control {inputGenerateJumlahPaket} inputJumlahPaket {input_is_dihitung} {classPaket}"></select>'
        			+'        </div>'
        			+'    </div>';

        			//set paket makanan
        			$(".trPaketMakanan .listPaket").empty();
        			if(MASTER_PAKET.makanan.length === 0) {
        				$(".trPaketMakanan").hide();
        			} else {
        				// $(".trPaketMakanan").show();
        				$.each(MASTER_PAKET.makanan, function(index ,item) {
        					let one = template + '';

        					let strNama = item.nama;
        					if(item.url_media) {
        						strNama = `<a target='_blank' class="imagePopup" href="${item.url_media}">${item.nama}</a>`;
        					}
        					
							one = JQCustom.replaceAll('{namaPaket}', strNama, one);
        					one = JQCustom.replaceAll('{idPaket}', item.id, one);
        					one = JQCustom.replaceAll('{hargaPaket}', item.harga, one);
        					one = JQCustom.replaceAll('{classPaket}', 'inputJumlahPaketMakanan', one);
        					one = JQCustom.replaceAll('{is_dihitung}', item.is_dihitung, one);
        					var is_dihitung = parseInt(item.is_dihitung) === 1;
        					one = JQCustom.replaceAll('{input_is_dihitung}', is_dihitung ? 'input_is_dihitung' : '', one);
        					one = JQCustom.replaceAll('{inputGenerateJumlahPaket}', is_dihitung ? 'inputGenerateJumlahPaket' : '', one);

        					$(".trPaketMakanan .listPaket").append(one);
        				});
        			}
        			//set paket snack
        			$(".trPaketSnack .listPaket").empty();
        			if(MASTER_PAKET.snack.length === 0) {
        				$(".trPaketSnack").hide();
        			} else {
        				// $(".trPaketSnack").show();
        				$.each(MASTER_PAKET.snack, function(index ,item) {
        					let one = template + '';

        					let strNama = item.nama;
        					var is_dihitung = item.is_dihitung;
        					if(item.url_media) {
        						strNama = `<a target='_blank' class="imagePopup" href="${item.url_media}">${item.nama}</a>`;
        					}
        					one = JQCustom.replaceAll('{namaPaket}', strNama, one);
        					one = JQCustom.replaceAll('{idPaket}', item.id, one);
        					one = JQCustom.replaceAll('{hargaPaket}', item.harga, one);
        					one = JQCustom.replaceAll('{classPaket}', 'inputJumlahPaketSnack', one);
        					one = JQCustom.replaceAll('{is_dihitung}', item.is_dihitung, one);
        					var is_dihitung = parseInt(item.is_dihitung) === 1;
        					one = JQCustom.replaceAll('{input_is_dihitung}', is_dihitung ? 'input_is_dihitung' : '', one);
        					one = JQCustom.replaceAll('{inputGenerateJumlahPaket}', is_dihitung ? 'inputGenerateJumlahPaket' : '', one);

        					$(".trPaketSnack .listPaket").append(one);
        				});
        			}
        			generateJumlah('inputJumlahPaket', limitJumlahPaket); //ini jumlah paket kecil
        		}
        	})

        	generateJumlah('selectJumlahPorsiSedekah', limitJumlahPaket);
        }

        initFormSedekah();

        function hitungTotalJumlahPaket() {
        	let listInput = $(".inputJumlahPaket");
			
        	let totalJumlahMakanan = 0;
        	let totalNilaiRupiah = 0;

        	$.each(listInput, function (index ,item) {
        		let me = $(item);
        		let isMakanan = me.hasClass('inputJumlahPaketMakanan');
        		var is_dihitung = me.hasClass('input_is_dihitung');

				let jumlahMakanan = 0;
				if(me.val()) {
					jumlahMakanan = parseInt(me.val());
				}
				if(is_dihitung) {
        			totalJumlahMakanan += jumlahMakanan;
        		}
				let harga = parseInt(me.data('harga'));
				totalNilaiRupiah += (jumlahMakanan * harga);
        	})

        	$(".inputTotalJumlahMakanan").val(totalJumlahMakanan);
        	$(".inputTotalJumlahUang").val(totalNilaiRupiah);

        	$(".totalJumlahMakanan").text(JQCustom.numberFormatIndonesia(totalJumlahMakanan));
        	$(".totalJumlahUang").text(JQCustom.numberFormatIndonesia(totalNilaiRupiah));

			let theOption = `.selectJumlahPorsiSedekah option[value="${totalJumlahMakanan}"`;
			if($(theOption).length === 0) {
				$(".selectJumlahPorsiSedekah").append(`<option value="${totalJumlahMakanan}" selected="selected">${totalJumlahMakanan}</option>`);
			}
			$(theOption).val(totalJumlahMakanan);
        };
        $(document).on("change", ".inputJumlahPaket", function (e) {
        	hitungTotalJumlahPaket();	
        })

		// $('#table-detail-per-tgl').DataTable().clear().destroy();
        $(document).on("click", ".btnLihatDonasiAll", function (e) {
        	e.preventDefault();

        	let me = $(this),
        	tipe = parseInt(me.data('tipe'));


			let tableId = '#table-donasi-all';
			if($.fn.dataTable.isDataTable(tableId)) {
				$(tableId).DataTable().clear().destroy();
				$(tableId).empty();
			}

			let url = '/wp-json/sedekah/all/' + tipe;
			let title = 'List Donasi';
			
			let columns = [];
			columns.push({'title': 'Tgl', 'data': 'tgl'})
			columns.push({'title': 'Donatur', 'data': 'donasi_oleh'})
			columns.push({title: 'Donasi', data: 'jenis_donasi_nama'});
			// columns.push({title: 'Jumlah Rupiah', data: 'jumlah_rupiah', render: JQCustom.numberFormatIndonesia, class: 'text-right'});
			// columns.push({title: 'Jumlah Porsi', data: 'jumlah_porsi', render: JQCustom.numberFormatIndonesia, class: 'text-right'});
			columns.push({title: 'Jumlah Rupiah', data: 'jumlah_rupiah', class: 'text-right'});
			columns.push({title: 'Jumlah Porsi', data: 'jumlah_porsi', class: 'text-right'});
			columns.push({title: 'Konfirmasi', data: 'konfirmasi_at'});
			columns.push({title: 'Konfirmasi Oleh', data: 'konfirmasi_oleh'});
			columns.push({title: 'Note Konfirmasi', data: 'konfirmasi_note'});
			columns.push({title: 'Diterima', data: 'diterima_at'});
			columns.push({title: 'DiTerima Oleh', data: 'diterima_oleh'});
			columns.push({title: 'Note Diterima', data: 'diterima_note'});

			if(tipe === 2) {
				title += ' - Detail';
				columns.push({title:'Nama Paket', data: 'nama_paket'});
				columns.push({title:'Jumlah Pesan', data: 'jumlah_pesan_paket', class: 'text-right'});
				columns.push({title:'Harga ', data: 'harga_paket', class: 'text-right'});
				columns.push({title:'SubTotal', data: 'subtotal', class: 'text-right'});
				// columns.push({title:'Jumlah Pesan', data: 'jumlah_pesan_paket', render: JQCustom.numberFormatIndonesia, class: 'text-right'});
				// columns.push({title:'Harga ', data: 'harga_paket', render: JQCustom.numberFormatIndonesia, class: 'text-right'});
				// columns.push({title:'SubTotal', data: 'subtotal', render: JQCustom.numberFormatIndonesia, class: 'text-right'});
			}
			columns.push({title: 'Telp Donatur', data: 'donatur_telp'});
			columns.push({title: 'Alamat Donatur', data: 'donatur_alamat'});

			$(tableId).DataTable( {
				"paging":   false,
				"bInfo" : false,
				ajax: {
					url: url,
					dataSrc: 'data',
					processing: true,
					},
				dom: 'frtipB',
				buttons: [
					{
						extend: 'excelHtml5',
						title: title
					},
					{
						extend: 'pdfHtml5',
						title: title
					}
				],
				columns: columns
			} );


			modalDonasiAll.show();
		});

        $(document).on("click", ".btnLihatDonaturAll", function (e) {
        	e.preventDefault();

        	let me = $(this),
        	tipe = parseInt(me.data('tipe'));

			let tableId = '#table-donasi-all';
			if($.fn.dataTable.isDataTable(tableId)) {
				$(tableId).DataTable().clear().destroy();
				$(tableId).empty();
			}

			let url = '/wp-json/donatur/all/' + tipe;
			let title = 'List Donatur';
			
			let columns = [];
			columns.push({title: 'Nama', data: 'nama'});
			columns.push({title: 'Nama Samaran', data: 'nama_samaran'});
			columns.push({title: 'Telp', data: 'telp'});
			columns.push({title: 'Alamat', data: 'alamat'});
			columns.push({title: 'Akun Admin', render:  function ( data, type, row, meta ) {
				let jenis = '';
				if(row.is_relawan == "1") {
					jenis = 'Ya';
				}
				return jenis;
			  }});
			columns.push({title: 'Pertama Donasi', data: 'created_at'});


			$(tableId).DataTable( {
				"paging":   false,
				"bInfo" : false,
				ajax: {
					url: url,
					dataSrc: 'data',
					processing: true,
					},
				dom: 'frtipB',
				buttons: [
					{
						extend: 'excelHtml5',
						title: title
					},
					{
						extend: 'pdfHtml5',
						title: title
					}
				],
				columns: columns,
				bAutoWidth: false,
				"order": [
					[ 4, "desc" ],
					[ 0, "asc" ]
				]
			} );


			modalDonasiAll.show();
		});

		// $('#table-detail-per-tgl').DataTable().clear().destroy();
        $(document).on("click", ".progress-parent", function (e) {
        	e.preventDefault();

        	let me = $(this),
        	jumlah = me.data('jumlah'),
            tgl_show = me.data('tgl_show'),
        	tgl = me.data('tgl');


			let tableId = '#table-detail-per-tgl';
			if($.fn.dataTable.isDataTable(tableId)) {
				$(tableId).DataTable().clear().destroy();
			}
			
			
			let newTable = '';
			newTable += '<thead>';
			newTable += '    <tr>';
			newTable += '        <th>No</th>';
			newTable += '        <th>Nama Donatur</th>';
			newTable += '        <th>Jenis Donasi</th>';
			newTable += '        <th class="text-right">Jumlah Porsi</th>';
			newTable += '        <th class="khususRelawan-off">Sudah Konfirmasi</th>';
			newTable += '        <th class="khususRelawan-off">Sudah Diterima</th>';
			newTable += '    </tr>';
			newTable += '</thead>';
			newTable += '<tbody class="tbody-list-detail-per-tgl">';
			newTable += '</tbody>';
			$('#table-detail-per-tgl').html(newTable);

        	let url = urlSedekahGetByTanggal.replace('{tgl}', tgl);

        	$.get(url)
        	.done(function function_name(result) {
        		if(result.status) {
        			modalSedekahPerTgl.show();
        			let modal = $("#modal-sedekah-per-tgl");
        			let title = modal.find(".modal-title");

        			title.html(`Daftar Sedekah: ${tgl_show}`);

        			let table = $("#table-detail-per-tgl");
        			let body = modal.find(".modal-body");
        			let tbody = body.find(".tbody-list-detail-per-tgl");
                                tbody.empty();

					$(".btnExportTableToExcel").data("tgl", tgl_show);

                    if(result.data.length === 0) {
                        let templateKosong = `<tr class="tr-info">`
                        + `<td class="" colspan="7">`
                        + `<div class="alert alert-warning">`
                        + `Belum ada yang menyumbang di tanggal ini. Ini kesempatan untuk Anda bisa menyumbang sekarang. Ayo berlomba untuk bersedekah.`
                        + `</div>`
                        + `</td>`
                        + `</tr>`
                        ;

                        tbody.append(templateKosong);
                        return;
                    }

        			let template = `<tr class="tr-info {isMine}">`
        			+ `<td>{no}</td>`
        			+ `<td><span class="textNamaDonatur">{donatur_nama}</span></td>`
        			+ `<td><span class="textJenisDonasi">{jenis_donasi_nama}</span></td>`
        			+ `<td class="text-right">{action} <span class="textJumlahPorsi">{jumlah_porsi}</span></td>`
        			+ `<td class="text-center khususRelawan-off {classSudahKonfirmasi}">{sudahKonfirmasi}</td>`
        			+ `<td class="text-center khususRelawan-off {classSudahDiterima}">{sudahDiterima}</td>`
        			+ `</tr>`
        			;
        			$.each(result.data, function function_name(index, item) {
						let isRelawan = localStorage.isRelawan;

        				let isMine = item.donatur_id === localStorage.donaturId;

        				let one = template + '';
        				one = JQCustom.replaceAll('{no}', JQCustom.numberFormatIndonesia(index + 1), one);
        				
						if(isRelawan && !isMine) {
							let noTelp = JQCustom.telpWA(item.donatur_telp);
							let donaturLinkWA = `<a target="_blank" href="https://wa.me/${noTelp}">`
								+ item.donatur_nama
								+ `</a>`;
							;
							one = JQCustom.replaceAll('{donatur_nama}', donaturLinkWA, one);
						} else {
							if(isMine) {
								one = JQCustom.replaceAll('{donatur_nama}', '<i>' + item.donatur_nama + ' (Anda)</i>', one);
							} else {
								one = JQCustom.replaceAll('{donatur_nama}', item.donatur_nama, one);
							}
						}
        				one = JQCustom.replaceAll('{jenis_donasi_nama}', item.jenis_donasi_nama, one);
        				one = JQCustom.replaceAll('{isMine}', isMine ? 'tr-is-mine' : '', one);
        				one = JQCustom.replaceAll('{jumlah_porsi}', JQCustom.numberFormatIndonesia(parseInt(item.jumlah_porsi)), one);

						let styleDisplayNone = 'style="display: none;"';
						let textSUDAH = item.konfirmasi_at ? 'SUDAH' : '';
						let hideTextKonfirmasi = item.konfirmasi_at ? '' : styleDisplayNone;
						let hideBtnKonfirmasi = item.konfirmasi_at ? styleDisplayNone : '';

						let textSudahKonfirmasi = `<span ${hideTextKonfirmasi} class="textSudahKonfirmasi${item.id} textSudahKonfirmasi">${textSUDAH}</span>`;
						let btnKonfirmasi = `<span ${hideBtnKonfirmasi} data-id="${item.id}" data-proses="konfirmasi" class="btnKonfirmasiDonasi${item.id} btnKonfirmasiDonasi btn btn-primary">`;
						btnKonfirmasi += 'Konfirmasi?';
						btnKonfirmasi += '</span>';
						if(!localStorage.isRelawan) {
							btnKonfirmasi = '';
						}
						let sudahKonfirmasi = textSudahKonfirmasi + btnKonfirmasi;
						one = JQCustom.replaceAll('{sudahKonfirmasi}', sudahKonfirmasi, one);
						let classSudahKonfirmasi = item.konfirmasi_at ? 'bg-success' : '';
						one = JQCustom.replaceAll('{classSudahKonfirmasi}', classSudahKonfirmasi, one);

						textSUDAH = item.diterima_at ? 'SUDAH' : '';
						let hideTextTerima = item.diterima_at ? '' : styleDisplayNone;
						let hideBtnDiterima = item.diterima_at ? styleDisplayNone : '';
						let textSudahDiterima = `<span ${hideTextTerima} class="textSudahDiterima${item.id} textSudahDiterima">${textSUDAH}</span>`;
						let btnSudahDiterima = `<span ${hideBtnDiterima} data-id="${item.id}" data-proses="diterima" class="btnDiterimaDonasi${item.id} btnDiterimaDonasi btn btn-primary">`;
						btnSudahDiterima += 'Terima?';
						btnSudahDiterima += '</span>';
						if(!localStorage.isRelawan) {
							btnSudahDiterima = '';
						}
						let sudahDiterima = textSudahDiterima + btnSudahDiterima;
						one = JQCustom.replaceAll('{sudahDiterima}', sudahDiterima, one);
						let classSudahDiterima = item.diterima_at ? 'bg-success' : '';
						one = JQCustom.replaceAll('{classSudahDiterima}', classSudahDiterima, one);

        				let action = '';
						action += `<span class="mr-2 float-left btn btn-info btnLihatDetailDonasi khususRelawan" data-id="${item.id}" >Detail</span>`;
        				if(isMine || isRelawan) {
							let btnClass = 'btn-warning';
							if(isRelawan) {
								if(!isMine) {
									btnClass = 'btn-danger';
								}
							}
        					action += `<span class="mr-2 float-left btn ${btnClass} btn-hapus-sedekah" data-donasi_id="${item.id}" >Hapus</span>`;
        				}

        				one = JQCustom.replaceAll('{action}', action, one);

        				tbody.append(one);
        			});



					$(tableId).DataTable( {
						"paging":   false,
						"bInfo" : false,
						dom: 'frtipB',
						buttons: [
							{
								extend: 'excelHtml5',
								title: tgl_show
							},
							{
								extend: 'pdfHtml5',
								title: tgl_show
							}
						]
					} );
			

					
					showHideKhususRelawan();
        		}
        	})
        })

        $(document).on("click", ".btnLihatDetailDonasi", function (e) {
        	e.preventDefault();
			
			let me = $(this),
			id = me.data("id");

        	let url = urlSedekahGetById.replace('{id}', id);
        	$.get(url)
        	.done(function function_name(result) {
				if(result.status) {
					let row = result.data;

					let noTelp = JQCustom.telpWA(row.donatur_telp);
					let donaturLinkWA = `<a target="_blank" href="https://wa.me/${noTelp}">`
						+ row.donatur_telp
						+ `</a>`;
					;

					let jenisDonasiDanUang = row.jenis_donasi_nama;
					if(row.jenis_donasi == 2) {
						jenisDonasiDanUang = row.jenis_donasi_nama + ' <span class="font-weight-bold "> Rp.<span class="detailDonasiTotalUang">' + JQCustom.numberFormatIndonesia(row.jumlah_pesan)  + '</span></span>' ;
					}

					$(".tdNamaDonatur").html(row.donatur_nama);
					$(".tdTelpDonatur").html(donaturLinkWA);
					$(".tdJenisDonasi").html(jenisDonasiDanUang);
					$(".tdJumlahPorsi").html(row.jumlah_porsi);

					let konfirmasi_at = row.konfirmasi_at;
					let diterima_at = row.diterima_at;
					let konfirmasi_oleh = row.konfirmasi_oleh;
					let diterima_oleh = row.diterima_oleh;

					let konfirmasi = konfirmasi_oleh ? konfirmasi_oleh + ' / ' + konfirmasi_at : '';
					let diterima = diterima_oleh ? diterima_oleh + ' / ' + diterima_at : '';

					$(".tdKonfirmasi").html(konfirmasi);
					$(".tdDiterima").html(diterima);

					let tableDetail = $(".tableDetail");
					let tbodyDetail = $(".tbodyDetail");
					//no, nama donasi, jumlah 
					let detail = result.detail;
					if(detail && detail.length > 0) {
						tableDetail.show();
					} else {
						tableDetail.hide();
					}
					tbodyDetail.empty();
					let totalAll = 0;
					$.each(detail, function name(index, item) {

						// id: "1",
						// donasi_id: "2",
						// paket_id: "1",
						// jumlah_pesan: "0",
						// jumlah_realisasi: "0",
						// is_active: "1",
						// nama: "Paket 15ribu",
						// is_makanan_pokok: "1",
						// is_dihitung: "1",
						// harga: "15000",
						// keterangan: "Ini adalah paket makanan Rp.15.000",
						// url_media: "https://bakubantu.com/wp-content/uploads/2021/03/paket-makanan-15-ribu.jpeg",
						// paket_is_active: "1"

						let subTotal = item.harga * item.jumlah_pesan;
						totalAll += subTotal;

						let newTr = '<tr>'
						+ `<td>${index + 1}</td>`
						+ `<td><a class="imagePopup" href="${item.url_media}" target="_blank" class="btn-link">${item.nama}</a></td>`
						+ `<td  class="text-right">${JQCustom.numberFormatIndonesia(item.jumlah_pesan)}</td>`
						+ `<td  class="text-right">${JQCustom.numberFormatIndonesia(item.harga)}</td>`
						+ `<td  class="text-right">${JQCustom.numberFormatIndonesia(subTotal)}</td>`
						+ '</tr>';
						tbodyDetail.append(newTr);							
					});

					$(".detailDonasiTotalUang").html(JQCustom.numberFormatIndonesia(totalAll));

					let newTrAll = '<tr>'
					+ `<td  class="font-weight-bold "></td>`
					+ `<td  class="font-weight-bold ">Total</td>`
					+ `<td  class="font-weight-bold text-right"></td>`
					+ `<td  class="font-weight-bold text-right"></td>`
					+ `<td  class="font-weight-bold text-right">${JQCustom.numberFormatIndonesia(totalAll)}</td>`
					+ '</tr>';
					tbodyDetail.append(newTrAll);

					modalDetailSedekah.show();
					modalSedekahPerTgl.hide();
				} else {
					modalShow("Load data gagal, silahkan coba lagi");
				}
			});
		});
        $(document).on("click", ".btnCloseDetailSedekah", function (e) {
        	e.preventDefault();

			modalSedekahPerTgl.show();
			modalDetailSedekah.hide();
		});


        $(document).on("click", ".btnCloseKonfirmasiSedekah", function (e) {
        	e.preventDefault();
			modalFormKonfirmasiSedekah.hide();
			modalSedekahPerTgl.show();
		});

        $(document).on("click", ".btnKonfirmasiDonasi, .btnDiterimaDonasi", function (e) {
        	e.preventDefault();

			let me = $(this),
				id = me.data("id"),
				proses = me.data("proses"),
				tr = me.closest("tr"),
				textNamaDonatur = tr.find(".textNamaDonatur").html(),
				textJenisDonasi = tr.find(".textJenisDonasi").html(),
				textJumlahPorsi = tr.find(".textJumlahPorsi").html(),
				formKonfirmasiSedekah = $(".formKonfirmasiSedekah")
				;

			let modal = $("#modalKonfirmasiSedekah");
			let modalTitle = modal.find(".modal-title");
			let labelKonfirmasi = modal.find(".labelKonfirmasi");

			formKonfirmasiSedekah.data("proses", proses);


			if(proses === "diterima") {
				modalTitle.text("Sedekah Diterima");
				labelKonfirmasi.text("Diterima");
			} else {
				//default
				modalTitle.text("Konfirmasi Sedekah");
				labelKonfirmasi.text("Konfirmasi");
			}

			$(".tdKonfirmasiNamaDonatur").html(textNamaDonatur);
			$(".tdKonfirmasiJenisDonasi").html(textJenisDonasi);
			$(".tdKonfirmasiJumlahPorsi").html(textJumlahPorsi);
			$(".inputKonfirmasiId").val(id);
			$(".inputKonfirmasiProses").val(proses);

			modalSedekahPerTgl.hide();
			modalFormKonfirmasiSedekah.show();
		});

        $(document).on("submit", ".formKonfirmasiSedekah", function (e) {
        	e.preventDefault();

			let me = $(this),
			data = me.serializeArray(),
			proses = me.find(".inputKonfirmasiProses").val()
			;

			let url = urlSedekahKonfirmasi;
			if(proses === "diterima") {
				url = urlSedekahDiterima;
			}


			$.post(url, data)
			.done(function name(result) {
				// modalShow("Sukses");
				if(result.status) {
					let id = $(".inputKonfirmasiId").val();

					if(proses === "diterima") {
						$(".textSudahDiterima" + id).show();
						$(".btnDiterimaDonasi" + id).hide();
					} else {
						$(".textSudahKonfirmasi" + id).show();
						$(".btnKonfirmasiDonasi" + id).hide();
					}

					modalFormKonfirmasiSedekah.hide();
					modalSedekahPerTgl.show();
				} else {
					modalShow(result.message || 'Maaf proses gagal, silahkan coba lagi');
				}
			});
		});

        $(document).on("click", ".btn-hapus-sedekah", function (e) {
            let me = $(this);
            let donasiId = me.data('donasi_id');
            let tr = me.closest("tr");


            if(!confirm(`Apakah Anda yakin ingin menghapus data ini?`)) return false;

        	let data = {
        		id: donasiId,
        	};
        	$.post(urlSedekahDeleteMine, data)
        	.done(function function_name(result) {
        		if(result.status) {
        			tr.remove();
        			// window.location.reload();
        			initTableSedekah();
        		} else {
        			modalShow(result.message || 'Maaf terjadi kesalahan, silahkan coba lagi');
        		}
        	})
        });

        $(document).on("click", ".link-tgl", function (e) {
        	e.preventDefault();


        	$(".inputJumlahPaket").val("");

        	let me = $(this),
        	jumlah = me.data('jumlah'),
        	is_spesial = me.data('spesial'),
        	tanggal = me.data('tgl');

			let is_disabled = me.hasClass('disabled');

			if(localStorage.isRelawan) {
				//do nothing 
			} else {
				if(is_disabled) {
					return;
				}

				jumlah = parseInt(jumlah);
				if(jumlah <= 0) {
					return;
				}
			}

			

			initFormSedekah(jumlah, is_spesial);

        	let inputTanggal = $('.inputTanggal');
        	inputTanggal.val(tanggal);

        	let listTanggal = tanggal.split('-');
        	let DMY = listTanggal[2] + '-' + listTanggal[1] + '-' + listTanggal[0];
        	let info = `Tanggal: ${DMY}`;
        	$(".info-sedekah").html(info);

			generateJumlah('selectJumlahPorsiSedekah', jumlah);
        	$(".selectJumlahPorsiSedekah").val(jumlah);	
        	$(".inputTanggal").val(tanggal);	

			$(".inputJenisDonasi1").prop("checked", true);
			$(".inputJenisDonasi1").trigger("change");

        	$.when(getSessionDonatur()).then(function(result) {
        		modalFormSedekah.show();
        		if(result.status) {
        			if(result.data.donaturId) {
        				let dataSession = result.data;

        				$(".inputNama").val(dataSession.donaturNama);
        				$(".inputNamaSamaran").val(dataSession.donaturNamaSamaran);

        				$(".inputGunakanNamaSamaran").prop('checked', dataSession.donaturNama != dataSession.donaturNamaSamaran);
        				$(".inputGunakanNamaSamaran").trigger('change');

        				$(".inputTelp").val(dataSession.donaturTelp);
        				$(".inputAlamat").val(dataSession.donaturAlamat);


        				info += `<br/>`;	
        			}
        		}
        	})
        })

        $(document).on("change", ".inputJenisDonasi", function (e) {
        	let me = $(this),
        	val = parseInt(me.val()),
        	isChecked = me.is(':checked');
			$(".trPaket").hide();

			let trJumlahDonasi = $(".trJumlahDonasi");

			if(val === 2) {
        		let showTotal = false;
        		if(MASTER_PAKET.makanan && MASTER_PAKET.makanan.length > 0) {
        			showTotal = true;
        			$(".trPaketMakanan").show();
        		}
        		if(MASTER_PAKET.snack && MASTER_PAKET.snack.length > 0) {
        			showTotal = true;
        			$(".trPaketSnack").show();
        		}

        		if(showTotal) {
        			$(".trPaketTotal").show();
        		}

				trJumlahDonasi.hide();
        	} else {
				trJumlahDonasi.show();
			}
        })

        function setLoggedInDonatur(donatur) {

			let isRelawan = localStorage.isRelawan;

        	let logos = `Selamat Datang ${donatur.nama} <br/>`;

        	if(donatur.namaSamaran && donatur.nama != donatur.namaSamaran) {
        		logos += `Nama Samaran: ${donatur.namaSamaran} <br/>`;
        	}
        	logos += `Telp: ${donatur.telp} <br/>`;
        	logos += `Alamat: ${donatur.alamat} <br/>`;
        	logos += `<br/>`;
        	logos += `<a href="${urlSedekahMine}" class="mb-2 btn btn-primary btn-lg mr-2"><i class="fas fa-praying-hands"></i> Sedekah Anda</a>`;
        	logos += `<a href="${urlProfileEdit}" class="mb-2 btn btn-primary btn-lg mr-2"><i class="fas fa-person"></i> Edit Profile</a>`;
            logos += `<a href="${urlDonaturLogout}" class="mb-2 btn btn-warning btn-lg btn-logout-donatur"><i class="fas fa-sign-out-alt"></i> Log Out</a>`;

			if(isRelawan) {
				// logos += '<div class="mt-3 mb-3">';
				logos += '<span class="mb-3 btn btn-primary btn-lg mr-2 btnLihatDonasiAll btnLihatDonasiAll1" data-tipe="1">';
				logos += 'List Donasi';
				logos += '</span>';

				logos += '<span class="mb-3 btn btn-primary btn-lg mr-2 btnLihatDonasiAll btnLihatDonasiAll2" data-tipe="2">';
				logos += 'List Donasi Detail';
				logos += '</span>';

				logos += '<a href="/donasi/all/daily" class="mb-3 btn btn-primary btn-lg mr-2 btnLihatDonasiSummaryDaily">';
				logos += 'Report Daily';
				logos += '</a>';
				
				logos += '<span class="mb-3 btn btn-primary btn-lg mr-2 btnLihatDonaturAll btnLihatDonaturAll1" data-tipe="1">';
				logos += 'List Donatur';
				logos += '</span>';
				// logos += '</div>';
					// logos += '<div class="container mt-4">';
					// logos += '<div class="row mt-3 text-left">';
					// 	logos += '<div class="col-md-12">';
					// 	logos += '</div>';
					// logos += '</div>';
				// logos += '</div>';
			}

        	let infoProfileDonatur = $(".infoProfileDonatur");
        	infoProfileDonatur.html(logos);


        	$(".dataDonaturLoggedin").show();
        	$(".dataDonaturNotLoggedin").hide();
        }

        $(document).on("submit", ".form-login-donatur", function(e) {
        	e.preventDefault();

        	let me = $(this),
        	data = me.serializeArray();

        	$.post(urlDonaturLogin, data)
        	.done(function function_name(result) {
        		if(result.status) {
        			let param = {
        				body: 'Login Berhasil!',
        				autoclose: 5,
        				showCountDown: 1,
        			};
        			modalShow(param);

        			let donatur = result.data;
        			localStorage.donaturId = donatur.id;
        			localStorage.donaturNama = donatur.nama;
        			localStorage.donaturNamaSamaran = donatur.nama_samaran;
        			localStorage.donaturTelp = donatur.telp;
        			localStorage.donaturAlamat = donatur.alamat;
        			localStorage.isRelawan = parseInt(data.isRelawan) === 1 ? 1 : '';

        			// setLoggedInDonatur(donatur);
					window.location.reload();

        		} else {
        			let msg = result.message || 'Maaf terjadi kesalahan, silahkan coba lagi.';
        			modalShow(msg);
        		}
        	});
        });

        $(document).on("submit", ".form-sedekah", function(e) {
        	e.preventDefault();

        	let me = $(this),
        	inputDonaturId = me.find('.inputDonaturId').val(),
        	inputJenisDonasi = parseInt(me.find('.inputJenisDonasi:checked').val()),
        	inputTanggal = me.find('.inputTanggal').val(),
        	inputNama = me.find('.inputNama').val(),
        	inputGunakanNamaSamaran = me.find('.inputGunakanNamaSamaran').is(':checked'),
        	elNamaSamaran = me.find('.inputNamaSamaran'),
            inputNamaSamaran = elNamaSamaran.val(),
        	inputTelp = me.find('.inputTelp').val(),
        	inputAlamat = me.find('.inputAlamat').val();


        	let totalJumlahMakanan = parseInt($(".inputTotalJumlahMakanan").val());
			// inputJumlah = parseInt(me.find('.selectJumlahPorsiSedekah').val()),
			let inputJumlah = totalJumlahMakanan;


        	if(inputJenisDonasi === 2) { //2 = uang
        		if(inputJumlah !== totalJumlahMakanan) {
        			let tampilAtas = JQCustom.numberFormatIndonesia(inputJumlah);
        			let tampilBawah = JQCustom.numberFormatIndonesia(totalJumlahMakanan);

        			alert(`Maaf, Jumlah di atas (${tampilAtas}) dan Total Jumlah di bawah tidak sama (${tampilBawah}).`);
        			return false;
        		}
        	}

        	if(!inputGunakanNamaSamaran) {
        		inputNamaSamaran = inputNama;
                elNamaSamaran.val(inputNama);
        	}

        	let data = {
        		'inputDonaturId': inputDonaturId,
        		'inputJenisDonasi': inputJenisDonasi,
        		'inputTanggal': inputTanggal,
        		'inputJumlah': inputJumlah,
        		'inputNama': inputNama,
        		'inputNamaSamaran': inputNamaSamaran,
        		'inputTelp': inputTelp,
        		'inputAlamat': inputAlamat,
        	};
        	modalFormSedekah.hide();    

        	let url = urlSedekahSave;
            let formData = me.serializeArray();
        	$.post(url, formData)
        	.done(function(result) {
        		if(result.status) {
                                let donatur = result.data.donatur;
                                localStorage.donaturId = donatur.id;
                                localStorage.donaturNama = donatur.nama;
                                localStorage.donaturNamaSamaran = donatur.nama_samaran;
                                localStorage.donaturTelp = donatur.telp;
                                localStorage.donaturAlamat = donatur.alamat;
                                initDataProfile();

        			let donaturKodeAkses = result.data.donaturKodeAkses;
        			let msg = 'Berhasil! Ini adalah Kode Akses Anda, Mohon dicatat kode ini agar Anda bisa login. \n\n'
        			+ `<br/>`
        			+ `<p><h3>Kode Akses: <label class="text-danger">${donaturKodeAkses}</label></h3></p>`
        			+ `<br/>`
        			+ `<p class="small font-italic">Kode Akses ini bisa digunakan jika Anda ingin membatalkan / merubah pesanan Anda.</p>`
        			;

					msg = `Terima kasih <b>${donatur.nama}</b>. Donasi anda sudah tercatat. `
					+ "<br/>Jika donasi bentuk uang, silakan transfer ke : "
					+ "<br/>"
					+ "<br/>BCA (014)"
					+ "<br/>An Zakaria Nasution"
					+ "<br/>No rek. 4660154731"
					+ "<br/>"
					+ "<br/>Harap wa bukti pembayaran ke : +62811787867"
					+ "<br/>"
					+ `<br/>Untuk melihat / edit / hapus / nambah  / kurang,  donasi dapat login kembali dengan nomer hp terdaftar anda di nomor ${donatur.telp}`
					+ "<br/>"
					+ "<br/>Silakan screen shot notifikasi ini."
					+ "<br/>"
					+ "<br/>ttd"
					+ "<br/>Panitia Takjil CPT"
					+ "";
        			
        			modalShow(msg, function () {
        				//window.location.reload();
	        			initTableSedekah();
        			});
        		} else {
        			alert(result.message);
        		}
        	});
        })

        $(document).on("change", ".inputGunakanNamaSamaran", function function_name(e) {
        	e.preventDefault();

        	let me = $(this),
        	isChecked = me.is(":checked");

        	let inputNamaSamaran = $(".inputNamaSamaran");

        	if(isChecked) {
        		$(".tr-donatur-nama-samaran").show();
        		inputNamaSamaran.focus();
        		inputNamaSamaran.prop("required", true);
        	} else {
        		inputNamaSamaran.prop("required", false);

        		$(".tr-donatur-nama-samaran").hide();
        	}
        })

        $(document).on("click", ".btnExportTableToExcel", function function_name(e) {
        	e.preventDefault();

			let me =$(this),
			tgl = me.data('tgl');

			let table = $("#table-detail-per-tgl");
			var tab_text = table.html();
			tab_text = '<table>' + tab_text + '</table>';
			tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
			tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
			tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params
		
			var fileName = tgl + '.xls'
			JQCustom.exportHtmlToExcel(tab_text, fileName);

			// var fileName = tgl + '.pdf'
			// JQCustom.exportHtmlToPDF(tab_text, fileName);

        })

    };

    function runScript() {
        // Script that does something and depends on jQuery being there.
        // if( window.$ ) {
	if( typeof jQuery !== 'undefined' ) {
        	window.$ = jQuery;
        	runScript2();
	} else {
            window.setTimeout( runScript, 100 );
        }
    }

    runScript();