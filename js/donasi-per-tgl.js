let urlSedekahMine = '/sedekah-mine';
let urlProfileEdit = '/profile-edit';

let urlSedekahIndex = '/wp-json/sedekah/index';
let urlSedekahMineApi = '/wp-json/sedekah/mine';
let urlSedekahSave = '/wp-json/sedekah/save';
let urlSedekahDeleteMine = '/wp-json/sedekah/delete/mine';
let urlSedekahGetByTanggal = '/wp-json/sedekah/sedekahGetByTanggal?tgl={tgl}';
let urlSedekahGetById = '/wp-json/sedekah/sedekahGetById?id={id}';
let urlSedekahKonfirmasi = '/wp-json/sedekah/konfirmasi';
let urlSedekahDiterima = '/wp-json/sedekah/diterima';

let urlDonaturSession = '/wp-json/donatur/session';
let urlDonaturLogin = '/wp-json/donatur/login';
let urlDonaturLogout = '/donatur-logout';
let urlMasterPaket = '/wp-json/master/paket';

let MASTER_PAKET = [];

let getSessionDonatur = function() {
	return $.get(urlDonaturSession);
}

function runScript2(params) {
	$(document).on("click", ".btn-go", function name(e) {
		e.preventDefault();

		var tgl = $('.input-tgl').val();

		if(!tgl) {
			modalShow('Tgl adalah mandatory!');
			return;
		}

		var date = new Date(tgl);
		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();

		let dmy = [day, month, year].join('-');
		let ymd = [year, month, day].join('-');

		let tableId = '#table-donasi-per-tgl';
		if($.fn.dataTable.isDataTable(tableId)) {
			$(tableId).DataTable().clear().destroy();
			$(tableId).empty();
		}

		let urlPaket = '/wp-json/paket/nama';
		$.get(urlPaket).done(function name(result) {
			if(!result.status) {
				modalShow(result.message || 'Data gagal diload, silahkan coba lagi');
				return;
			}

			let url = '/wp-json/sedekah/summary/tgl';
			let title = 'List Donasi Tgl: ' + dmy;
						
			let columns = [];
			columns.push({'title': 'Tgl', 'data': 'tgl'})
			columns.push({'title': 'Jumlah Porsi', 'data': 'jumlah_porsi'})
			columns.push({'title': 'Jumlah Porsi', 'data': 'jumlah_rupiah'})
			
			//Tgl | Jumlah Porsi | Paket A, PAKET B, PAKET C 
			// $.each(result.data, function name(index, item) {
			// 	columns.push({title: item.nama, data: 'paket_' + item.id});
			// })

			let param = {
				tgl: ymd
			}
			$(tableId).DataTable( {
				"paging":   false,
				"bInfo" : false,
				ajax: {
					url: url,
					dataSrc: 'data',
					processing: true,
					"type": 'GET',
               		"data": param
					},
				dom: 'frtipB',
				buttons: [
					{
						extend: 'excelHtml5',
						title: title
					},
					{
						extend: 'pdfHtml5',
						title: title
					}
				],
				columns: columns
			} );
		});
	})
}

function runScript() {
	// Script that does something and depends on jQuery being there.
	// if( window.$ ) {
	if( typeof jQuery !== 'undefined' ) {
			window.$ = jQuery;
			runScript2();
	} else {
		window.setTimeout( runScript, 100 );
	}
}

runScript();