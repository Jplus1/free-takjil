<?php /* Template Name: Sedekah Mine */ ?>

<?php
global $wpdb;

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">


        <article id="post-234" class="post-234 page type-page status-publish hentry">
            <header class="entry-header">
                <a id="page-title"></a>
                <h1 class="entry-title">Sedekah</h1>
            </header><!-- .entry-header -->

            <div class="entry-content">
                <div class="row">
                    <div class="col-md-6 col-sm-12 float-right pull-right">
                        <div class="card">
                            <div class="card-header">
                                Data Anda
                            </div>
                            <div class="card-body cardDataAnda">
                                <div class="dataDonaturNotLoggedin ">
                                    <p>
                                        Ketik Kode Akses Anda disini untuk login. 
                                    </p>
                                    <div class="row">
                                        <div class="col-md-8 col-6">
                                            <form class="form-login-donatur" >
                                                <div class="input-group mb-3">
                                                  <input type="text" class="form-control" placeholder="Kode Akses" name="kodeAkses" required="required">
                                                  <div class="input-group-append">
                                                    <input type="submit" class="btn btn-outline-secondary" value="Login"/>
                                                  </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-md-4 col-6">

                                        </div>
                                    </div>
                                </div>
                                <div class="dataDonaturLoggedin infoProfileDonatur" style="display: none;">
                                    
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6 col-sm-12 float-left pull-left" >
                        <div class="card">
                            <div class="card-header">
                                Cara bersedekah:
                            </div>
                            <div class="card-body">
                                1. Klik pada tanggal yang Anda inginkan untuk menyumbang.<br/>
                                2. Isi sedekah Anda.<br/>
                                3. Simpan.<br/>
                                <br/>
                                <br/>
                                <a class="btn btn-success btn-lg" href="/sedekah">Klik Disini</a> untuk bersedekah
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row row-semua row-1">
                    <div class="col-12">

                        <table class="table table-hover table-list-sedekah" data-mine="1">
                            <thead>
                                <tr>
                                    <th class="kolom-tgl">Hari / Tgl</th>
                                    <th >Sedekah Makanan</th>
                                    <th >Sedekah Uang</th>
                                    <th >Total</th>
                                    <th ></th>
                                </tr>
                            </thead>
                            <tbody class="tbody-list-sedekah">
                           </tbody>
                       </table>
                   </div>
               </div>

           </div><!-- .entry-content -->
           <footer class="entry-meta">
           </footer>
       </article>

   </main><!-- #main -->
</div><!-- #primary -->

<?php 
    $versi_resource = VERSI_RESOURCE; 
?>

<script type="text/javascript" src="/wp-content/themes/radiate/js/sedekah-mine.js?v=<?=$versi_resource?>"></script>
<?php get_sidebar(); ?>
<?php get_footer(); ?>