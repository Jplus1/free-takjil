<?php
//v.0.2 - frontend

namespace radiate;

defined("CONS_DEV_MODE") or define("CONS_DEV_MODE", 1);

$path_root = $_SERVER['DOCUMENT_ROOT'];
$base_url = isset($base_url) ? $base_url : "";
$base_url or $base_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://".$_SERVER["HTTP_HOST"];

defined("UTIL_PATH_ROOT") or define("UTIL_PATH_ROOT", $path_root);
defined("UTIL_BASE_URL") or define("UTIL_BASE_URL", $base_url);

//Util Frontend
class Util {

    /*
     * @param <p>int $num \n
     * $case
     * Constant CASE_
     * </p>
     * @return String
     */
    public static function randomString($num = 5, $case = CASE_RANDOM) {
        if($case == CASE_UPPER) {
            $seed = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                .'0123456789');
        } else if($case == CASE_LOWER) {
            $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                .'0123456789');
        } else if($case == CASE_NUMBER) {
            $seed = str_split('0123456789');
        } else {
            $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                .'0123456789');
        }
        shuffle($seed);
        $rand = '';
        foreach (array_rand($seed, $num) as $k) $rand .= $seed[$k];
        return $rand;
    }

    //cara lama
    //frontend -> $_POST["userId"] //karena dari FE, bisa dimanipulasi gampang bgt
    //gateway -> semua $_POST di teruskan ke BE
    //BE -> mengambil userId dari $_POST

    //cara yang di propose
    //frontend -> $_POST paramter2 yg dibutuhkan aja, minus userId //
    //gateway -> $_POST["userId"] di isi dengan $_SESSION["userId']/. benar2 ID Milik user yang sedang login
    //BE -> mengambil userId dari $_POST


    public static function getUserID($throw_on_error = false) {
        $USER_ID = static::getPost("userId"); //untuk mengakomodir sistemnya vencap, jadi pakai ini
        $USER_ID or $USER_ID = static::getPost("SESSION_USER_ID"); //ini otomatis ditambahkan oleh gw.php

        //untuk kebutuhan testing di POSTMAN
        $USER_ID or $USER_ID = static::getPost("TEMPORARY_USER_ID");//

        if(!$USER_ID && $throw_on_error) {
            static::throwErrorUserID();
        }

        return $USER_ID;
    }

    public static function getRoleID($throw_on_error = false) {
        $ROLE_ID = static::getPost("SESSION_ROLE_ID");

        //untuk kebutuhan testing di POSTMAN
        $ROLE_ID or $ROLE_ID = static::getPost("TEMPORARY_ROLE_ID");

        if(!$ROLE_ID && $throw_on_error) {
            static::throwErrorUserID();
        }

        return $ROLE_ID;
    }


    public static function getRequest($key = null, $default = null) {
        $value = isset($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
        return $value;
    }
    public static function getRequestInt($key = null, $default = null) {
        return intval(static::getRequest($key, $default));
    }

    public static function getPost($key = null, $default = null) {
        $value = isset($_POST[$key]) ? $_POST[$key] : $default;
        return $value;
    }
    public static function getPostInt($key = null, $default = null) {
        return intval(static::getPost($key, $default));
    }

    public static function getGet($key = null, $default = null) {
        $value = isset($_GET[$key]) ? $_GET[$key] : $default;
        return $value;
    }
    public static function getGetInt($key = null, $default = 0) {
        return intval(static::getGet($key, $default));
    }


    public static function sessionStart() {
        // if (session_status() == PHP_SESSION_NONE) session_start();
        if (!session_id()) session_start();
    }

    public static function getSession($key = null, $default = null) {
        static::sessionStart();

        $value = isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
        return $value;
    }
    public static function getSessionInt($key = null, $default = null) {
        $value = static::getSession($key, $default);
        return intval($value);
    }

    public static function setSession($key, $value = "")
    {
        static::sessionStart();
        $_SESSION[$key] = $value;
    }

    public static function getBaseURL() {
        $base_url = isset($base_url) ? $base_url : "";
        $base_url or $base_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://" . $_SERVER["HTTP_HOST"];

        return $base_url;
    }

    //ini khusus buat tahun di atas 2000
    public static function date_DMonY_to_Ymd($DMonY) {
        $Ymd = "";


        $split = explode("-", $DMonY);
        if(count($split) == 3) {
            $d = $split[0];
            $dd = str_pad(intval($d), 2, "0");

            $mon = $split[1];
            $y = $split[2];

            $Y = intval($y);
            if($Y < 2000) {
                $Y += 2000;
            }

            $mm = static::monToMm($mon);

            $Ymd = "$Y-$mm-$dd";
        }

        return $Ymd;
    }

    public static function date_dmY_to_Ymd($DMonY) {
        $Ymd = "";

        $split = explode("-", $DMonY);
        if(count($split) == 3) {
            $d = $split[0];
            $dd = str_pad(intval($d), 2, "0");
            $mm = $split[1];
            $Y = $split[2];

            $Ymd = "$Y-$mm-$dd";
        }

        return $Ymd;
    }

    public static function nowOracle() {
        return date('d-M-y h.i.s.u A');
    }
    public static function oracleDate($ymd, $throw_on_error = true) {
        $array = explode("-", $ymd);
        if(count($array) != 3) {
            if($throw_on_error) {
                $msg = "Date format is invalid: $ymd";

                static::throwError($msg);
            } else {
                return null;
            }
        }

        $yyyy = $array[0];
        $mm = $array[1];
        $mon = static::mmToMon($mm);
        $dd = $array[2];

        $format_indonesia = "$dd-$mon-$yyyy";
        return $format_indonesia;
    }

    public static function mmToMon($mm) {
        $m = intval($mm);
        switch ($m) {
            case 1:return "Jan"; break;
            case 2:return "Feb"; break;
            case 3:return "Mar"; break;
            case 4:return "Apr"; break;
            case 5:return "May"; break;
            case 6:return "Jun"; break;
            case 7:return "Jul"; break;
            case 8:return "Aug"; break;
            case 9:return "Sep"; break;
            case 10:return "Oct"; break;
            case 11:return "Nop"; break;
            case 12:return "Dec"; break;
            default: die("Month is invalid: $mm"); break;
        }
    }

    public static function monToMm($mon) {
        $upperMon = strtoupper($mon);
        switch ($upperMon) {
            case "JAN":return 1; break;
            case "FEB":return 2; break;
            case "MAR":return 3; break;
            case "APR":return 4; break;
            case "MAY":return 5; break;
            case "JUN":return 6; break;
            case "JUL":return 7; break;
            case "AUG":
            case "AGU":
                return 8;
                break;
            case "SEP":return 9; break;
            case "OKT":
            case "OCT":
                return 10;
                break;
            case "NOP":
            case "NOV":
                return 11;
                break;
            case "DEC":
            case "DES":
                return 12;
                break;
            default: die("Month is invalid: $mon"); break;
        }
    }


    public static function getArr($array, $key, $default = null)
    {
        return static::getArrOrObject($array, $key, $default);
    }
    public static function setArr(&$arrayOrObject, $key, $value)
    {
        $step = [];
        $step[] = __LINE__;
        $step[] = $key;
        $step[] = $value;
        $step[__LINE__ . " key = $key"] = $value;

        if(is_array($arrayOrObject)) {
            $arrayOrObject[$key] = $value;
            $step[] = __LINE__ . " | IS_ARRAY = true";
            $step[] = $arrayOrObject;
        } elseif(is_object($arrayOrObject)) {
            $step[] = __LINE__ . " | IS_OBJECT = true";
            if(property_exists($arrayOrObject, $key)) {
                $step[] = __LINE__;
                $arrayOrObject->$key = $value;
            }
        }
        $result = Util::defaultResult();
        $result["step"] = $step;
        return $result;
    }


    public static function getArrOrObject($arrayOrObject, $key, $default = null)
    {
        $res = $default;
        if(is_array($arrayOrObject)) {
            $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
            if($res == null) {
                $key = strtoupper($key);
                $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
                if($res == null) {
                    $key = strtolower($key);
                    $res = isset($arrayOrObject[$key]) ? $arrayOrObject[$key] : $default;
                }
            }
        } else {
            if(is_object($arrayOrObject)) {
                if(property_exists($arrayOrObject, $key) || isset($arrayOrObject->$key)) {
                    $res = $arrayOrObject->$key;
                }
            }
        }
        return $res;
    }
    public static function explodeCSV($csv, $delimiter = ",") {
        return static::csvToArray($csv, $delimiter);
    }
    public static function csvToArray($csv, $delimiter = ",")
    {
        $delimiter or $delimiter = ",";

        $array = explode($delimiter, $csv);
        $new_array = [];
        foreach($array as $one) {
            $new_array[] = trim($one);
        }
        return $new_array;
    }
    public static function inList($key, $array)
    {
        $key = strtoupper(trim($key));
        $res = false;
        foreach($array as $value) {
            if(is_string($value)) {
                $value = strtoupper(trim($value));
            }
            if($key == $value) {
                $res = true;
                break;
            }
        }
        return $res;
    }

    public static function inArrayKey($key, $array)
    {
        $key = strtoupper(trim($key));
        $res = false;
        foreach($array as $key2=>$value) {
            if($key == strtoupper(trim($key2))) {
                $res = true;
                break;
            }
        }
        return $res;
    }

    public static function getArrayKeyOnly($array) {
        $keys = [];
        foreach($array as $key=>$value) {
            $keys[] = $key;
        }
        return $keys;
    }

    public static function getAllKeywordInBraces($string, $leftDelimiter = "\{", $rightDelimiter = "\}") {
        $pattern = '#'. $leftDelimiter. '(.*?)'. $rightDelimiter .'#';
        preg_match_all($pattern, $string, $matches);
        return $matches[0];
    }

    public static function hasProp($prop, $obj)
    {
        $res = false;
        if(is_object($obj)) {
            $res = property_exists($obj, $prop);
            if(!$res) {
                $prop = strtoupper($prop);
                $res = property_exists($obj, $prop);
                if(!$res) {
                    $prop = strtolower($prop);
                    $res = property_exists($obj, $prop);
                }
            }
        }
        return $res;
    }

    public static function goDie($msg, $data = null, $code = "") {
        $bt = debug_backtrace();
        $caller = array_shift($bt);
        $logline = static::getArr($caller, "file") . ":" . static::getArr($caller, "line");

        echo "Program Stopped at: $logline";
        echo "\n################################<hr/>\n";
        echo $msg;
        echo "\n################################<hr/>\n";
        if($data) print_r($data);
        echo "\n################################<hr/>\n";
        die($code);
    }

    public static function transBegin($MODEL) {
        $bt = debug_backtrace();
        $caller = array_shift($bt);
        $logline = static::getArr($caller, "file") . ":" . static::getArr($caller, "line");

        $stringLog = "$logline => Trans Begin";
        static::addGlobalData("trans_status", $stringLog, false);

        return $MODEL::getDb()->beginTransaction();
    }

    public static function transCommit($trans) {
        $bt = debug_backtrace();
        $caller = array_shift($bt);
        $logline = static::getArr($caller, "file") . ":" . static::getArr($caller, "line");

        $stringLog = "$logline => Trans commit";
        static::addGlobalData("trans_status", $stringLog, false);

        try {
            $trans->commit();
            return static::defaultResult();
        } catch (\Exception $e) {
            return static::defaultResult(false, $e->getMessage());
        }
    }

    public static function transRollback($trans) {
        $bt = debug_backtrace();
        $caller = array_shift($bt);
        $logline = static::getArr($caller, "file") . ":" . static::getArr($caller, "line");

        $stringLog = "$logline => Trans rollback";
        static::addGlobalData("trans_status", $stringLog, false);

        $trans->rollback();
    }

    public static function defaultErrorUserID() {
        $msg = "USER_ID is required. Make sure you are accessing this API via route.php";
        return static::defaultResult(false, $msg);
    }
    public static function defaultResult($default_status = true, $msg = "", $data = null) {
        $result = [];
        $result["status"] = $default_status;
        $result["message"] = $msg;
        if(!is_null($data)) {
            $result["data"] = $data;
        }
        if(CONS_DEV_MODE) {
            $bt = debug_backtrace();
            $caller = array_shift($bt);
            $logline = static::getArr($caller, "file") . ":" . static::getArr($caller, "line");
            $result["logline"] = $logline;
        }
        return $result;
    }
    public static function default404($msg = "") {
        try {
            http_response_code(404);
            echo $msg;
            die();
        } catch (\Exception $e) {
            die($e->getTraceAsString());
        }
    }

    public static function likeString($haystack, $needle) {
        return strpos($haystack, $needle) !== false;
    }

    public static function throwErrorUserID($msg = "No Access!", $code = 401) {
        static::throwError($msg, $code);
//        header("HTTP/1.0 401 $msg");
//        echo $msg;
//        exit(401);
    }

    public static function throwError($msg = "", $code = 500) {
        try {
            header("HTTP/1.0 $code $msg");
            echo $msg;
            exit();
//            throw new \yii\web\HttpException(500, $msg);
        } catch (\Exception $e) {
            die($e->getTraceAsString());
        }
    }

    public static function controllerCodeToRoute($controllerCode) {
        switch ($controllerCode) {
            case "location/index": $result = "doclocation/index"; break;
            case "location/all": $result = "doclocation/all"; break;
            case "location/create": $result = "doclocation/create"; break;
            case "location/update": $result = "doclocation/update"; break;
            case "location/delete": $result = "doclocation/delete"; break;
            case "location/undelete": $result = "doclocation/undelete"; break;
            case "location/active": $result = "doclocation/active"; break;
            case "location/inactive": $result = "doclocation/inactive"; break;
            case "location/hello": $result = "doclocation/hello"; break;

            default : $result = ""; break;
        }

        return $result;
    }


    public static function dirCreate($path, $mode = null, $recursive = true)
    {
        $result = static::defaultResult();
        if (file_exists($path)) return $result;

        try {
            $mode = $mode ? $mode : 0755;
            $ok1 = mkdir($path, $mode, $recursive);
            $ok2 = chmod($path, $mode);

            $result["status"] = $ok1 && $ok2;
        } catch (\Exception $e) {
            $result["status"] = false;
            $result["message"] = "Failed to create dir: $path";
            $result["message_system"] = $e->getTraceAsString();
        }
        return $result;
    }

    public static function getExtentionFromBase64($base64) {
        list($type, $base64) = explode(';', $base64);
        list(, $extension) = explode('/', $type);


        switch ($extension) {
            case "msword":
            case "ms-doc":
                $extension = "doc";
                break;
            case "vnd.openxmlformats-officedocument.wordprocessingml.document":
                $extension = "docx";
                break;
            case "msexcel":
            case "excel":
                $extension = "xls";
                break;
            case "octet-stream":
            case "vnd.ms-excel":
            case "x-excel":
            case "x-msexcel":
            case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                $extension = "xlsx";
                break;
            case "mspowerpoint":
            case "powerpoint":
            case "vnd.ms-powerpoint":
                $extension = "ppt";
                break;
            case "x-mspowerpoint":
            case "vnd.openxmlformats-officedocument.presentationml.presentation":
                $extension = "pptx";
                break;
        }

        return $extension;
    }

    public static function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }


    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public static function replaceStart($haystack, $needle) {
        return substr($haystack, strlen($needle)); // "quick brown fox jumps over the lazy dog."
    }
    public static function replaceEnd($haystack, $needle) {
        return substr($haystack, 0, - strlen($needle));
    }

    public static function cleanEmail($string) {
        return preg_replace("/[^A-Za-z0-9@.]/", '', $string);
    }
    public static function cleanAlpha($string) {
        return preg_replace("/[^A-Za-z]/", '', $string);
    }
    public static function cleanAlphaNumeric($string) {
        return preg_replace("/[^A-Za-z0-9]/", '', $string);
    }
    public static function cleanFirebase($string) {
        return preg_replace("/[^A-Za-z0-9.-]/", '', $string);
    }
    public static function cleanNumeric($string) {
        return preg_replace("/[^0-9]/", '', $string);
    }
    public static function cleanCSVDelimiter($string) {
        $string = str_replace("(", "", $string);
        $string = str_replace(")", "", $string);
        return $string;
    }
    public static function isJson($string) {
        $result = false;
        if(is_string($string)) {
            json_decode($string);
            $result = (json_last_error() == JSON_ERROR_NONE);
        }
        return $result;
    }
    public static function jsonDecode($string, $assoc = false) {
        $result = false;
        if(is_string($string)) {
            try {
                $result = json_decode($string, $assoc);
                $sukses = (json_last_error() == JSON_ERROR_NONE);
                if(!$sukses) {
                    $result = false;
                }
            } catch (\Exception $e) {
                error_log("json decode error: " . __FILE__ . ":" . __LINE__);
                error_log($string);
                error_log($e->getTraceAsString());
            }
        }
        return $result;
    }
    public static function curlURL($url, $param = [], $timeout_detik = 20)
    {
        $CONS_DEV_MODE = CONS_DEV_MODE;
        $CONS_DEV_MODE or $CONS_DEV_MODE = isset($param["CONS_DEV_MODE"]) ? $param["CONS_DEV_MODE"] : "";
        $param["CONS_DEV_MODE"] = $CONS_DEV_MODE;

        $data_yang_dilempar = $param;
        $data_post = http_build_query($data_yang_dilempar);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_post);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if(defined("CURLOPT_SSL_VERIFYPEER")) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        if($timeout_detik) {
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout_detik); //timeout in seconds
        }
        $result = array();
        $curl_result = curl_exec($ch) or $result;

        $curl_message = "";
        if (curl_errno($ch)) {
            $result["status"] = false;
            $result["message"] = "API Connection failed.";
            if($CONS_DEV_MODE) {
                $curl_message = curl_error($ch);
            }
        } else {

            if(!static::isJson($curl_result)) {
                $result["status"] = false;
                $result["message"] = "Something went wrong, please try again. Code 1";
            } else {
                if ($curl_result) {
                    $result = $curl_result;
                    if (!is_array($curl_result)) {
                        $result = json_decode($curl_result, true);
                    }

                    $result["status"] = isset($result["status"]) ? $result["status"] : true;
                    $result["message"] = isset($result["message"]) ? $result["message"] : "";
                } else {
                    $result = $curl_result;
                    $result["status"] = false;
                    $result["message"] = "Something went wrong, please try again. Code 2";
                }
            }
        }
        curl_close($ch);

        if($CONS_DEV_MODE) {
            $result["url"] = $url;
            $result["param"] = $param;
            $result["curl_result"] = print_r($curl_result, true);
            $result["curl_message"] = $curl_message;
            $result["curl_data"] = $data_post;
        }
        $result["data"] = isset($result["data"]) ? $result["data"] : [];
        return $result;
    }

    public static function callGateway($controllerCode, $system = "DOCME", $param = []) {
        $url = HOST_GATEWAY;
        $param["system"] = $system;
        $param["controllerCode"] = $controllerCode;
        return static::curlURL($url, $param);
    }

    public static function phpinfo_array($return=false){
        /* Andale!  Andale!  Yee-Hah! */
        ob_start();
        phpinfo(-1);

        $pi = preg_replace(
            array('#^.*<body>(.*)</body>.*$#ms', '#<h2>PHP License</h2>.*$#ms',
                '#<h1>Configuration</h1>#',  "#\r?\n#", "#</(h1|h2|h3|tr)>#", '# +<#',
                "#[ \t]+#", '#&nbsp;#', '#  +#', '# class=".*?"#', '%&#039;%',
                '#<tr>(?:.*?)" src="(?:.*?)=(.*?)" alt="PHP Logo" /></a>'
                .'<h1>PHP Version (.*?)</h1>(?:\n+?)</td></tr>#',
                '#<h1><a href="(?:.*?)\?=(.*?)">PHP Credits</a></h1>#',
                '#<tr>(?:.*?)" src="(?:.*?)=(.*?)"(?:.*?)Zend Engine (.*?),(?:.*?)</tr>#',
                "# +#", '#<tr>#', '#</tr>#'),
            array('$1', '', '', '', '</$1>' . "\n", '<', ' ', ' ', ' ', '', ' ',
                '<h2>PHP Configuration</h2>'."\n".'<tr><td>PHP Version</td><td>$2</td></tr>'.
                "\n".'<tr><td>PHP Egg</td><td>$1</td></tr>',
                '<tr><td>PHP Credits Egg</td><td>$1</td></tr>',
                '<tr><td>Zend Engine</td><td>$2</td></tr>' . "\n" .
                '<tr><td>Zend Egg</td><td>$1</td></tr>', ' ', '%S%', '%E%'),
            ob_get_clean());

        $sections = explode('<h2>', strip_tags($pi, '<h2><th><td>'));
        unset($sections[0]);

        $pi = array();
        foreach($sections as $section){
            $n = substr($section, 0, strpos($section, '</h2>'));
            preg_match_all(
                '#%S%(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?(?:<td>(.*?)</td>)?%E%#',
                $section, $askapache, PREG_SET_ORDER);
            foreach($askapache as $m)
                $pi[$n][$m[1]]=(!isset($m[3])||$m[2]==$m[3])?$m[2]:array_slice($m,2);
        }

        return ($return === false) ? print_r($pi) : $pi;
    }

    public static function dieNoAccess($msg = "No Access") {
        die("
        <div class='error'>
        $msg
        </div>
        <style>
            body {
                background: #000;
            }
            .error {
                display: block; border: red solid 1px; padding: 20px; margin: 200px auto; width: 400px; color: red; text-align: center;           }
        </style>
        ");
    }


    public static function apiKeyCreate($salt = "", $minutePlus = 0)
    {
        $minutePlus = intval($minutePlus);
        $time = date("YmdHi", strtotime("+$minutePlus minutes"));
        $time2 = date("Y-m-d H:i", strtotime("+$minutePlus minutes"));
        $md5Salt = md5($salt);
        $md5Time = md5($time);
        $result = md5($md5Salt . $md5Time);

        $isGwDebug = defined("PREGW_DEBUG") && PREGW_DEBUG;
        if($isGwDebug) {
            $logos = "minutePlus: $minutePlus | time: $time2 | salt: $salt | result: $result \n";
            echo $logos;
        }
        return $result;
    }

    public static function apiKeyCheck($apiKey, $salt = "", $minuteTolerance = 5)
    {
        $isGwDebug = defined("PREGW_DEBUG") && PREGW_DEBUG;
        $step = [];
        $step[] = __LINE__;
        $apiKeyServer = static::apiKeyCreate($salt);
        $result = $apiKey == $apiKeyServer;
        if (!$result) {
            $step[] = __LINE__;
            $minuteTolerance = intval($minuteTolerance);
            for ($x = 1; $x <= $minuteTolerance; $x++) {
                $step[] = __LINE__;
                $serverKey = static::apiKeyCreate($salt, -1 * $x);
                $result = $apiKey == $serverKey;

                $logos = ( __LINE__ . " | x=". (-1*$x) ." | apiKey: $apiKey | serverKey: $serverKey | result: " . $result . " \n");
                $step[] = $logos;
                if($isGwDebug) echo $logos;

                if ($result) {
                    break;
                }

                $serverKey = static::apiKeyCreate($salt, $x);
                $result = $apiKey == $serverKey;

                $logos = ( __LINE__ . " | x=". $x." | apiKey: $apiKey | serverKey: $serverKey | result: " . $result . " \n");
                $step[] = $logos;
                if($isGwDebug) echo $logos;

                if ($result) {
                    break;
                }
            }
        }
        return $result;
    }


    public static function getUserIpAddress(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
